VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DecimalEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'INTERFACES

Implements IType
Implements ITypeClonable
Implements ITypeComparable
Implements ITypeConvertible

'CONSTANTS

Private Const CONST_MAX As String = "79228162514264337593543950335"
Private Const CONST_MIN As String = "-79228162514264337593543950335"
Private Const CONST_SIZE As Long = 14&

'VARIABLES

Private m_Value As Variant

'EVENTS

Private Sub Class_Initialize()

    m_Value = CDec(0&)

End Sub

'PROPERTIES

Private Property Get IType_Kind() As Long

    IType_Kind = vbDecimal

End Property

Private Property Get IType_Pointer() As Long

    IType_Pointer = VarPtr(m_Value)

End Property

Private Property Get IType_Size() As Long

    IType_Size = CONST_SIZE

End Property

Private Property Get ITypeComparable_Equals(Value As Variant, ByVal CompareMethod As VbCompareMethod) As Long

    ITypeComparable_Equals = Compare_Any(m_Value, Value, CompareMethod)

End Property

Public Property Get Equals(ByRef Value As Variant, Optional ByVal CompareMethod As VbCompareMethod) As Long

    Equals = ITypeComparable_Equals(Value, CompareMethod)

End Property

Public Property Get Kind() As VbVarType

    Kind = IType_Kind

End Property

Public Property Get Max() As Variant

    Max = CDec(CONST_MAX)

End Property

Public Property Get Min() As Variant

    Min = CDec(CONST_MIN)

End Property

Public Property Get Pointer() As Long

    Pointer = IType_Pointer

End Property

Public Property Get Size() As Long

    Size = IType_Size

End Property

Public Property Get Value() As Variant
Attribute Value.VB_UserMemId = 0
Attribute Value.VB_MemberFlags = "200"

    Value = m_Value

End Property

Public Property Let Value(ByRef NewValue As Variant)

    On Error Resume Next

    m_Value = CDec(NewValue)

End Property

'METHODS

Private Function IType_Parse(Value As Variant) As IType

    Dim l_Number As String

    Set IType_Parse = Me

    On Error GoTo ErrorHandler

    If Not IsObject(Value) Then

        Select Case VarType(Value)

            Case vbDecimal

                m_Value = Value

            Case vbInteger To vbDate, vbBoolean, vbByte, vbError

                m_Value = CDec(Value)

            Case vbString

                l_Number = ConvertToNumeric(StrPtr(Value), Len(Value))

                m_Value = CDec(Left$(l_Number, 29& + (InStr(1&, l_Number, ChrW$(PUB_COMMA), vbBinaryCompare) > 0&) * CONST_L_NG))

            Case Else

                m_Value = CDec(0&)

        End Select

        Exit Function

    End If

ErrorHandler:

    m_Value = CDec(0&)

End Function

Private Function ITypeClonable_Clone() As IType

    Set ITypeClonable_Clone = ITypeConvertible_ToType(New DecimalEx)

End Function

Private Function ITypeConvertible_ToArray() As IType

    Dim l_List As ListEx

    Set l_List = New ListEx

    Set ITypeConvertible_ToArray = l_List.CreateFrom(Me)

    Set l_List = Nothing

End Function

Private Function ITypeConvertible_ToBoolean() As IType

    Set ITypeConvertible_ToBoolean = ITypeConvertible_ToType(New BooleanEx)

End Function

Private Function ITypeConvertible_ToByte() As IType

    Set ITypeConvertible_ToByte = ITypeConvertible_ToType(New ByteEx)

End Function

Private Function ITypeConvertible_ToCurrency() As IType

    Set ITypeConvertible_ToCurrency = ITypeConvertible_ToType(New CurrencyEx)

End Function

Private Function ITypeConvertible_ToDateTime() As IType

    Set ITypeConvertible_ToDateTime = ITypeConvertible_ToType(New DateTimeEx)

End Function

Private Function ITypeConvertible_ToDecimal() As IType

    Set ITypeConvertible_ToDecimal = Me

End Function

Private Function ITypeConvertible_ToDouble() As IType

    Set ITypeConvertible_ToDouble = ITypeConvertible_ToType(New DoubleEx)

End Function

Private Function ITypeConvertible_ToInteger() As IType

    Set ITypeConvertible_ToInteger = ITypeConvertible_ToType(New IntegerEx)

End Function

Private Function ITypeConvertible_ToLong() As IType

    Set ITypeConvertible_ToLong = ITypeConvertible_ToType(New LongEx)

End Function

Private Function ITypeConvertible_ToList() As IType

    Set ITypeConvertible_ToList = ITypeConvertible_ToType(New ListEx)

End Function

Private Function ITypeConvertible_ToSingle() As IType

    Set ITypeConvertible_ToSingle = ITypeConvertible_ToType(New SingleEx)

End Function

Private Function ITypeConvertible_ToString(Optional Format As String) As IType

    Set ITypeConvertible_ToString = New StringEx

    If Len(Format) Then ITypeConvertible_ToString.Parse Strings.Format$(m_Value, Format) Else ITypeConvertible_ToString.Parse m_Value

End Function

Private Function ITypeConvertible_ToType(Object As IType) As IType

    Set ITypeConvertible_ToType = Object.Parse(m_Value)

End Function

Public Function Clone() As DecimalEx

    Set Clone = ITypeClonable_Clone

End Function

Public Function Decrement() As DecimalEx

    Set Decrement = Me

    If m_Value >= Min + 1 Then m_Value = m_Value - 1

End Function

Public Function Increment() As DecimalEx

    Set Increment = Me

    If m_Value <= Max - 1 Then m_Value = m_Value + 1

End Function

Public Function Parse(ByRef Value As Variant) As DecimalEx

    Set Parse = IType_Parse(Value)

End Function

Public Function Range(ByRef MinValue As Variant, ByRef MaxValue As Variant) As DecimalEx

    Set Range = Me

    On Error GoTo ErrorHandler

    If m_Value > CDec(MaxValue) Then

        m_Value = CDec(MaxValue)

    ElseIf m_Value < CDec(MinValue) Then

        m_Value = CDec(MinValue)

    End If

ErrorHandler:

End Function

Public Function Round(ByVal DecimalDigits As Long) As DecimalEx

    Set Round = Me

    On Error Resume Next

    m_Value = Math.Round(m_Value, DecimalDigits)

End Function

Public Function ToArray() As ListEx

    Set ToArray = ITypeConvertible_ToArray

End Function

Public Function ToBoolean() As BooleanEx

    Set ToBoolean = ITypeConvertible_ToBoolean

End Function

Public Function ToByte() As ByteEx

    Set ToByte = ITypeConvertible_ToByte

End Function

Public Function ToCurrency() As CurrencyEx

    Set ToCurrency = ITypeConvertible_ToCurrency

End Function

Public Function ToDateTime() As DateTimeEx

    Set ToDateTime = ITypeConvertible_ToDateTime

End Function

Public Function ToDouble() As DoubleEx

    Set ToDouble = ITypeConvertible_ToDouble

End Function

Public Function ToInteger() As IntegerEx

    Set ToInteger = ITypeConvertible_ToInteger

End Function

Public Function ToList() As ListEx

    Set ToList = ITypeConvertible_ToList

End Function

Public Function ToLong() As LongEx

    Set ToLong = ITypeConvertible_ToLong

End Function

Public Function ToSingle() As SingleEx

    Set ToSingle = ITypeConvertible_ToSingle

End Function

Public Function ToString(Optional ByRef Format As String) As StringEx

    Set ToString = ITypeConvertible_ToString(Format)

End Function
