Attribute VB_Name = "Common"
Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------'
'
' LICENSE AGREEMENTS
'
' This software Is provided 'as-is', without any express or implied warranty. In no event will the author be held liable for any damages
' arising from the use of this software. Permission is granted to anyone to use this software for any purpose, including commercial
' applications, and to alter it and redistribute it freely, subject to the following restrictions:
' 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this
'    software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
' 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
'
'------------------------------------------------------------------------------------------------------------------------------------------'

'CONSTANTS

Public Const CONST_CHAR_32 As String = " "
Public Const CONST_L_NG As Long = -1&

Private Const CONST_NUMERIC_MAX_LEN As Long = 308&

'VARIABLES

Public PUB_COMMA As Integer
Private m_Minus As Integer

'METHODS

Private Sub Main()

    Dim l_Return As String

    If System_GetLocale(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, l_Return) Then PUB_COMMA = AscW(l_Return)

    If System_GetLocale(LOCALE_USER_DEFAULT, LOCALE_SNEGATIVESIGN, l_Return) Then m_Minus = AscW(l_Return)

End Sub

Public Function ConvertToNumeric(ByVal lPointer As Long, ByVal lLength As Long) As String

    Dim l_CharCode As Integer
    Dim l_CharCount As Long
    Dim l_HasComma As Boolean
    Dim l_HasMinus As Boolean
    Dim l_Iterator As Long
    Dim l_Pointer As Long

    If lLength Then

        ConvertToNumeric = Space$(lLength)

        l_Pointer = StrPtr(ConvertToNumeric)

        For l_Iterator = 1& To lLength

            Memory_Get2 ByVal lPointer, l_CharCode

            If l_CharCode > 47 And l_CharCode < 58 Then

                Memory_Put2 ByVal l_Pointer, ByVal l_CharCode

                l_CharCount = l_CharCount + 1&
                l_Pointer = l_Pointer + 2&

            ElseIf Not l_HasComma And l_CharCode = PUB_COMMA Then

                Memory_Put2 ByVal l_Pointer, ByVal l_CharCode

                l_CharCount = l_CharCount + 1&
                l_Pointer = l_Pointer + 2&
                l_HasComma = True

            ElseIf l_CharCount = 0& And Not l_HasComma And Not l_HasMinus And l_CharCode = m_Minus Then

                Memory_Put2 ByVal l_Pointer, ByVal l_CharCode

                l_CharCount = l_CharCount + 1&
                l_Pointer = l_Pointer + 2&
                l_HasMinus = True

            End If

            lPointer = lPointer + 2&

        Next l_Iterator

        If l_CharCount > CONST_NUMERIC_MAX_LEN Then

            ConvertToNumeric = Left$(ConvertToNumeric, CONST_NUMERIC_MAX_LEN)

        ElseIf l_CharCount > 1& Then

            ConvertToNumeric = Left$(ConvertToNumeric, l_CharCount)

        ElseIf l_CharCount = 1& Then

            If AscW(ConvertToNumeric) = PUB_COMMA Or AscW(ConvertToNumeric) = m_Minus Then ConvertToNumeric = ChrW$(48&) Else ConvertToNumeric = Left$(ConvertToNumeric, 1&)

        Else

            ConvertToNumeric = ChrW$(48&)

        End If

    Else

        ConvertToNumeric = ChrW$(48&)

    End If

End Function

Public Sub VariantInit(ByRef uVariable As Variant, ByVal lVariableType As Long)

    Memory_Copy uVariable, lVariableType, 2&

End Sub

Public Sub VariantZero(ByRef uVariable As Variant)

    Memory_Copy uVariable, 0&, 4&

End Sub
