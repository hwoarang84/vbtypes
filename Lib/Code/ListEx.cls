VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ListEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'INTERFACES

Implements IType
Implements ITypeClonable
Implements ITypeComparable

'CONSTANTS

Private Const CONST_DIMENSION As Long = 1&

'VARIABLES

Private m_Array As ArrayEx

'EVENTS

Private Sub Class_Initialize()

    Set m_Array = New ArrayEx

End Sub

Private Sub Class_Terminate()

    Set m_Array = Nothing

End Sub

'PROPERTIES

Private Property Get IType_Kind() As Long

    IType_Kind = vbArray + m_Array.ElementType

End Property

Private Property Get IType_Pointer() As Long

    IType_Pointer = m_Array.Pointer

End Property

Private Property Get IType_Size() As Long

    IType_Size = m_Array.Size

End Property

Private Property Get ITypeComparable_Equals(Value As Variant, ByVal CompareMethod As VbCompareMethod) As Long

    ITypeComparable_Equals = m_Array.Equals(Value, CompareMethod)

End Property

Public Property Get Count() As Long

    Count = m_Array.Elements(CONST_DIMENSION)

End Property

Public Property Get Data() As Long

    Data = m_Array.Data

End Property

Public Property Get ElementSize() As Long

    ElementSize = m_Array.ElementSize

End Property

Public Property Get ElementType() As VbVarType

    ElementType = m_Array.ElementType

End Property

Public Property Get Equals(ByRef Value As Variant, Optional ByVal CompareMethod As VbCompareMethod) As Long

    Equals = ITypeComparable_Equals(Value, CompareMethod)

End Property

Public Property Get Exists() As Boolean

    Exists = m_Array.Exists

End Property

Public Property Get First() As Variant

    First = Item(0&)

End Property

Public Property Get IndexOf(ByRef Value As Variant, Optional ByVal CompareMethod As VbCompareMethod, Optional ByVal UsePartialMatchForStrings As Boolean) As Long

    Dim l_End As Long
    Dim l_Index As Long
    Dim l_Start As Long
    Dim l_Value As Variant

    IndexOf = CONST_L_NG

    l_Index = Count

    If l_Index Then

        VariantInit l_Value, m_Array.ElementType

        If m_Array.Sorted Then

            l_End = l_Index + CONST_L_NG

            Do While l_Start <= l_End

                l_Index = (l_Start + l_End) \ 2&

                m_Array.ItemGetInternal l_Index, l_Value

                If Compare_Any(Value, l_Value, CompareMethod) = COMPARISON_EQUAL Then

                    IndexOf = l_Index

                    Exit Do

                ElseIf (m_Array.Sorted = 1& And Value > l_Value) Or (m_Array.Sorted = CONST_L_NG And Value < l_Value) Then

                    l_Start = l_Index + 1&

                Else

                    l_End = l_Index + CONST_L_NG

                End If

            Loop

        Else

            For l_Index = 0& To l_Index + CONST_L_NG

                m_Array.ItemGetInternal l_Index, l_Value

                If m_Array.ElementType = vbString Then

                    If Not UsePartialMatchForStrings Then l_End = Compare_String((Value), l_Value, CompareMethod) = COMPARISON_EQUAL Else l_End = InStr(1&, l_Value, Value, CompareMethod)

                    If l_End Then

                        IndexOf = l_Index

                        Exit For

                    End If

                ElseIf l_Value = Value Then

                    IndexOf = l_Index

                    Exit For

                End If

            Next l_Index

        End If

        VariantZero l_Value

    End If

End Property

Public Property Get Item(ByVal Index As Long) As Variant

    If Index >= 0& And Index < Count Then

        VariantInit Item, m_Array.ElementType

        If m_Array.ItemGetInternal(Index, Item) <> S_OK Then VariantZero Item

    End If

End Property

Public Property Let Item(ByVal Index As Long, ByRef NewValue As Variant)

    If Index >= 0& And Index < Count Then

        If m_Array.ItemSetInternal(Index, NewValue) = S_OK Then m_Array.Sorted = 0&

    End If

End Property

Public Property Get ItemExists(ByVal Index As Long) As Boolean

    If m_Array.Pointer Then ItemExists = m_Array.ItemExistsInternal(Index, CONST_DIMENSION)

End Property

Public Property Get Kind() As VbVarType

    Kind = IType_Kind

End Property

Public Property Get Last() As Variant

    Last = Item(Count + CONST_L_NG)

End Property

Public Property Get Pointer() As Long

    Pointer = IType_Pointer

End Property

Public Property Get Size() As Long

    Size = IType_Size

End Property

Public Property Get Sorted() As Long

    Sorted = m_Array.Sorted

End Property

Friend Property Let Sorted(ByVal NewValue As Long)

    m_Array.Sorted = NewValue

End Property

Public Property Get Value() As Variant
Attribute Value.VB_UserMemId = 0
Attribute Value.VB_MemberFlags = "200"

    Value = m_Array.Value

End Property

Public Property Let Value(ByRef NewValue As Variant)

    IType_Parse NewValue

End Property

'METHODS

Private Function IType_Parse(Value As Variant) As IType

    Set IType_Parse = Me

    m_Array.Destroy
    m_Array.AddRange Value

End Function

Private Function ITypeClonable_Clone() As IType

    Set ITypeClonable_Clone = New ListEx

    If m_Array.Pointer Then ITypeClonable_Clone.Parse m_Array.Value

End Function

Public Function Add(ByRef Item As Variant) As ListEx

    Set Add = Me

    m_Array.Add Item

End Function

Public Function AddRange(ByRef Range As Variant) As ListEx

    Set AddRange = Me

    m_Array.AddRange Range

End Function

Public Function Clone() As ListEx

    Set Clone = ITypeClonable_Clone

    Clone.Sorted = m_Array.Sorted

End Function

Public Function Concat(ByRef List As ListEx) As ListEx

    Set Concat = AddRange(List.Value)

End Function

Public Function Create(ByVal ArrayType As VbVarType, ByVal Count As Long) As ListEx

    Set Create = Me

    m_Array.Create ArrayType, Array(0&, Count + CONST_L_NG)

End Function

Public Function Destroy() As ListEx

    Set Destroy = Me

    m_Array.Destroy

End Function

Public Function Distinct() As ListEx

    Dim l_Found As Boolean
    Dim l_Index As Long
    Dim l_Iterator1 As Long
    Dim l_Iterator2 As Long
    Dim l_Value1 As Variant
    Dim l_Value2 As Variant
    Dim l_UBound As Long

    Set Distinct = Me

    l_UBound = Count + CONST_L_NG

    If l_UBound > 0& Then

        VariantInit l_Value1, m_Array.ElementType
        VariantInit l_Value2, m_Array.ElementType

        If m_Array.Sorted Then

            Do
                m_Array.ItemGetInternal l_Iterator1, l_Value1

                For l_Iterator2 = l_Iterator1 + 1& To l_UBound

                    l_Iterator1 = l_Iterator2

                    m_Array.ItemGetInternal l_Iterator2, l_Value2

                    If Compare_Any(l_Value1, l_Value2, vbBinaryCompare) Then

                        l_Index = l_Index + 1&

                        If l_Index < l_Iterator2 Then m_Array.ItemSetInternal l_Index, l_Value2

                        Exit For

                    End If

                Next l_Iterator2

            Loop While l_Iterator1 < l_UBound

        Else

            Do

                l_Found = False
                l_Index = l_Index + 1&

                m_Array.ItemGetInternal l_Index, l_Value1

                For l_Iterator2 = 0& To l_Iterator1

                    m_Array.ItemGetInternal l_Iterator2, l_Value2

                    If Compare_Any(l_Value1, l_Value2, vbBinaryCompare) = COMPARISON_EQUAL Then

                        l_Found = True

                        Exit For

                    End If

                Next l_Iterator2

                If Not l_Found Then

                    l_Iterator1 = l_Iterator1 + 1&

                    If l_Index > l_Iterator1 Then m_Array.ItemSetInternal l_Iterator1, l_Value1

                End If

            Loop While l_Index < l_UBound

            l_Index = l_Iterator1

        End If

        VariantZero l_Value1
        VariantZero l_Value2

        m_Array.Resize l_Index + 1&

    End If

End Function

Public Function Filter(ByRef Values As Variant) As ListEx

    Set Filter = Me

    If (VarType(Values) And vbArray) = vbArray Then FilterInternal Values, False, False Else FilterInternal Array(Values), False, False

End Function

Public Function FilterAt(ByRef Indexes As Variant) As ListEx

    Set FilterAt = Me

    If (VarType(Indexes) And vbArray) = vbArray Then FilterInternal Indexes, False, True Else FilterInternal Array(Indexes), False, True

End Function

Public Function NewEnum() As IEnumVARIANT
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    Set NewEnum = NewEnumerator(Me, Count + CONST_L_NG)

End Function

Public Function Parse(ByRef Value As Variant) As ListEx

    Set Parse = IType_Parse(Value)

End Function

Public Function Range(ByVal StartIndex As Long, ByVal EndIndex As Long) As ListEx

    Dim l_End As Long
    Dim l_Indexes() As Long
    Dim l_Start As Long

    Set Range = New ListEx

    If EnsureIndexes(StartIndex, EndIndex, l_Start, l_End) Then m_Array.ToListInternal Range, l_Start, l_End, l_Indexes, CONST_L_NG

End Function

Public Function Remove(ByRef Values As Variant) As ListEx

    Set Remove = Me

    If (VarType(Values) And vbArray) = vbArray Then FilterInternal Values, True, False Else FilterInternal Array(Values), True, False

End Function

Public Function RemoveAt(ByRef Indexes As Variant) As ListEx

    Set RemoveAt = Me

    If (VarType(Indexes) And vbArray) = vbArray Then FilterInternal Indexes, True, True Else FilterInternal Array(Indexes), True, True

End Function

Public Function Resize(ByVal ElementsCount As Long) As ListEx

    Set Resize = Me

    m_Array.Resize ElementsCount

End Function

Public Function Reverse() As ListEx

    Dim l_Count As Long
    Dim l_Index As Long
    Dim l_Value1 As Variant
    Dim l_Value2 As Variant

    Set Reverse = Me

    l_Count = Count + CONST_L_NG

    If l_Count >= 0& Then

        VariantInit l_Value1, m_Array.ElementType
        VariantInit l_Value2, m_Array.ElementType

        For l_Index = 0& To l_Count \ 2& - ((l_Count + 1&) Mod 2)

            m_Array.ItemGetInternal l_Index, l_Value1
            m_Array.ItemGetInternal l_Count - l_Index, l_Value2

            m_Array.ItemSetInternal l_Index, l_Value2
            m_Array.ItemSetInternal l_Count - l_Index, l_Value1

        Next l_Index

        VariantZero l_Value1
        VariantZero l_Value2

        m_Array.Sorted = m_Array.Sorted * CONST_L_NG

    End If

End Function

Public Function Sort(Optional ByVal Descending As Boolean, Optional ByVal StartIndex As Long = CONST_L_NG, Optional ByVal EndIndex As Long = CONST_L_NG) As ListEx

    Dim l_Compare As ENUM_COMPARISON
    Dim l_End As Long
    Dim l_Iterator1 As Long
    Dim l_Iterator2 As Long
    Dim l_Middle As Long
    Dim l_Start As Long
    Dim l_Temp As Variant
    Dim l_Value1 As Variant
    Dim l_Value2 As Variant

    Set Sort = Me

    On Error GoTo ErrorHandler

    If EnsureIndexes(StartIndex, EndIndex, l_Start, l_End) Then

        l_Middle = (l_Start + l_End) \ 2&
        l_Iterator1 = l_Start
        l_Iterator2 = l_End

        VariantInit l_Temp, m_Array.ElementType
        VariantInit l_Value1, m_Array.ElementType
        VariantInit l_Value2, m_Array.ElementType

        m_Array.ItemGetInternal l_Middle, l_Temp

        While l_Iterator1 <= l_Iterator2

            Do

                l_Middle = l_Iterator1

                m_Array.ItemGetInternal l_Middle, l_Value1

                l_Compare = Compare_Any(l_Value1, l_Temp, vbBinaryCompare)

                If ((Descending = False And l_Compare = COMPARISON_LESS) Or (Descending = True And l_Compare = COMPARISON_GREATER)) And l_Iterator1 < l_End Then l_Iterator1 = l_Iterator1 + 1& Else Exit Do

            Loop

            Do

                l_Middle = l_Iterator2

                m_Array.ItemGetInternal l_Middle, l_Value2

                l_Compare = Compare_Any(l_Temp, l_Value2, vbBinaryCompare)

                If ((Descending = False And l_Compare = COMPARISON_LESS) Or (Descending = True And l_Compare = COMPARISON_GREATER)) And l_Iterator2 > l_Start Then l_Iterator2 = l_Iterator2 + CONST_L_NG Else Exit Do

            Loop

            If l_Iterator1 <= l_Iterator2 Then

                m_Array.ItemSetInternal l_Middle, l_Value1

                l_Middle = l_Iterator1

                m_Array.ItemSetInternal l_Middle, l_Value2

                l_Iterator1 = l_Iterator1 + 1&
                l_Iterator2 = l_Iterator2 + CONST_L_NG

            End If

        Wend

        VariantZero l_Value1
        VariantZero l_Value2

        If l_Start < l_Iterator2 Then

            l_Middle = l_Start

            Sort Descending, l_Middle, l_Iterator2

        End If

        If l_Iterator1 < l_End Then

            l_Middle = l_Iterator1

            Sort Descending, l_Middle, l_End

        End If

        VariantZero l_Temp

        If StartIndex <> CONST_L_NG Or EndIndex <> CONST_L_NG Then m_Array.Sorted = 0& Else m_Array.Sorted = Descending + 1& + Descending

    End If

ErrorHandler:

End Function

Public Function ToString(Optional ByRef Delimeter As String, Optional ByVal StartIndex As Long = CONST_L_NG, Optional ByVal EndIndex As Long = CONST_L_NG) As StringEx

    Dim l_End As Long
    Dim l_Indexes() As Long
    Dim l_Start As Long

    Set ToString = New StringEx

    EnsureIndexes StartIndex, EndIndex, l_Start, l_End

    m_Array.ToStringInternal ToString, l_Start, l_End, l_Indexes, CONST_L_NG, Delimeter

End Function

Friend Function CreateFrom(ByRef Value As IType) As IType

    Set CreateFrom = Create(vbByte, Value.Size)

    Memory_Copy ByVal Data, ByVal Value.Pointer, Value.Size

End Function

Private Function EnsureIndexes(ByVal StartIndex As Long, ByVal EndIndex As Long, ByRef StartIndexRet As Long, ByRef EndIndexRet As Long) As Boolean

    EndIndexRet = Count + CONST_L_NG

    If EndIndexRet >= 0& Then

        If StartIndex > 0& And StartIndex <= EndIndexRet Then StartIndexRet = StartIndex

        If EndIndex >= StartIndexRet And EndIndex <= EndIndexRet Then EndIndexRet = EndIndex

        EnsureIndexes = True

    End If

End Function

Private Sub FilterInternal(ByRef Values As Variant, ByVal Remove As Boolean, ByVal ValuesAreIndexes As Boolean)

    Dim l_Index As Long
    Dim l_Iterator1 As Long
    Dim l_Iterator2 As Long
    Dim l_Found As Boolean
    Dim l_UBound As Long
    Dim l_Value As Variant

    l_UBound = UBound(Values)

    If l_UBound > CONST_L_NG Then

        VariantInit l_Value, m_Array.ElementType

        For l_Iterator1 = 0& To Count + CONST_L_NG

            m_Array.ItemGetInternal l_Iterator1, l_Value

            l_Found = False

            For l_Iterator2 = 0& To l_UBound

                If ValuesAreIndexes Then

                    If l_Iterator1 = Values(l_Iterator2) Then

                        l_Found = True

                        Exit For

                    End If

                Else

                    If Compare_Any(l_Value, Values(l_Iterator2), vbBinaryCompare) = COMPARISON_EQUAL Then

                        l_Found = True

                        Exit For

                    End If

                End If

            Next l_Iterator2

            If Remove <> l_Found Then

                If l_Iterator1 <> l_Index Then m_Array.ItemSetInternal l_Index, l_Value

                l_Index = l_Index + 1&

            End If

        Next l_Iterator1

        VariantZero l_Value

        m_Array.Resize l_Index

    End If

End Sub
