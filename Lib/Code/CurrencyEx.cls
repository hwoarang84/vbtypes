VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CurrencyEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'INTERFACES

Implements IType
Implements ITypeClonable
Implements ITypeComparable
Implements ITypeConvertible

'CONSTANTS

Private Const CONST_MAX As Currency = 922337203685477.5807@
Private Const CONST_MIN As Currency = -922337203685477.5807@
Private Const CONST_SIZE As Long = 8&

'VARIABLES

Private m_Value As Currency

'PROPERTIES

Private Property Get IType_Kind() As Long

    IType_Kind = vbCurrency

End Property

Private Property Get IType_Pointer() As Long

    IType_Pointer = VarPtr(m_Value)

End Property

Private Property Get IType_Size() As Long

    IType_Size = CONST_SIZE

End Property

Private Property Get ITypeComparable_Equals(Value As Variant, ByVal CompareMethod As VbCompareMethod) As Long

    ITypeComparable_Equals = Compare_Currency(m_Value, Value, CompareMethod)

End Property

Public Property Get Equals(ByRef Value As Variant, Optional ByVal CompareMethod As VbCompareMethod) As Long

    Equals = ITypeComparable_Equals(Value, CompareMethod)

End Property

Public Property Get Kind() As VbVarType

    Kind = IType_Kind

End Property

Public Property Get Max() As Currency

    Max = CONST_MAX

End Property

Public Property Get Min() As Currency

    Min = CONST_MIN

End Property

Public Property Get Pointer() As Long

    Pointer = IType_Pointer

End Property

Public Property Get Size() As Long

    Size = IType_Size

End Property

Public Property Get Value() As Currency
Attribute Value.VB_UserMemId = 0
Attribute Value.VB_MemberFlags = "200"

    Value = m_Value

End Property

Public Property Let Value(ByVal NewValue As Currency)

    m_Value = NewValue

End Property

'METHODS

Private Function IType_Parse(Value As Variant) As IType

    Dim l_Number As Double

    Set IType_Parse = Me

    If IsObject(Value) Then

        m_Value = 0@

    Else

        Select Case VarType(Value)

            Case vbBoolean, vbByte, vbCurrency, vbDate, vbError, vbInteger, vbLong

                m_Value = Value

            Case vbDecimal, vbDouble, vbSingle

                If Value > CONST_MIN Then

                    If Value < CONST_MAX Then m_Value = Value Else m_Value = CONST_MAX

                Else

                    m_Value = CONST_MIN

                End If

            Case vbString

                l_Number = ConvertToNumeric(StrPtr(Value), Len(Value))

                If l_Number > CONST_MIN Then

                    If l_Number < CONST_MAX Then m_Value = l_Number Else m_Value = CONST_MAX

                Else

                    m_Value = CONST_MIN

                End If

            Case Else

                m_Value = 0@

        End Select

    End If

End Function

Private Function ITypeClonable_Clone() As IType

    Set ITypeClonable_Clone = ITypeConvertible_ToType(New CurrencyEx)

End Function

Private Function ITypeConvertible_ToArray() As IType

    Dim l_List As ListEx

    Set l_List = New ListEx

    Set ITypeConvertible_ToArray = l_List.CreateFrom(Me)

    Set l_List = Nothing

End Function

Private Function ITypeConvertible_ToBoolean() As IType

    Set ITypeConvertible_ToBoolean = ITypeConvertible_ToType(New BooleanEx)

End Function

Private Function ITypeConvertible_ToByte() As IType

    Set ITypeConvertible_ToByte = ITypeConvertible_ToType(New ByteEx)

End Function

Private Function ITypeConvertible_ToCurrency() As IType

    Set ITypeConvertible_ToCurrency = Me

End Function

Private Function ITypeConvertible_ToDateTime() As IType

    Set ITypeConvertible_ToDateTime = ITypeConvertible_ToType(New DateTimeEx)

End Function

Private Function ITypeConvertible_ToDecimal() As IType

    Set ITypeConvertible_ToDecimal = ITypeConvertible_ToType(New DecimalEx)

End Function

Private Function ITypeConvertible_ToDouble() As IType

    Set ITypeConvertible_ToDouble = ITypeConvertible_ToType(New DoubleEx)

End Function

Private Function ITypeConvertible_ToInteger() As IType

    Set ITypeConvertible_ToInteger = ITypeConvertible_ToType(New IntegerEx)

End Function

Private Function ITypeConvertible_ToLong() As IType

    Set ITypeConvertible_ToLong = ITypeConvertible_ToType(New LongEx)

End Function

Private Function ITypeConvertible_ToList() As IType

    Set ITypeConvertible_ToList = ITypeConvertible_ToType(New ListEx)

End Function

Private Function ITypeConvertible_ToSingle() As IType

    Set ITypeConvertible_ToSingle = ITypeConvertible_ToType(New SingleEx)

End Function

Private Function ITypeConvertible_ToString(Optional Format As String) As IType

    Set ITypeConvertible_ToString = New StringEx

    If Len(Format) Then ITypeConvertible_ToString.Parse Strings.Format$(m_Value, Format) Else ITypeConvertible_ToString.Parse m_Value

End Function

Private Function ITypeConvertible_ToType(Object As IType) As IType

    Set ITypeConvertible_ToType = Object.Parse(m_Value)

End Function

Public Function Clone() As CurrencyEx

    Set Clone = ITypeClonable_Clone

End Function

Public Function Decrement() As CurrencyEx

    Set Decrement = Me

    If m_Value >= CONST_MIN + 1@ Then m_Value = m_Value - 1@

End Function

Public Function Increment() As CurrencyEx

    Set Increment = Me

    If m_Value <= CONST_MAX - 1@ Then m_Value = m_Value + 1@

End Function

Public Function Parse(ByRef Value As Variant) As CurrencyEx

    Set Parse = IType_Parse(Value)

End Function

Public Function Range(ByVal MinValue As Currency, ByVal MaxValue As Currency) As CurrencyEx

    Set Range = Me

    If m_Value > MaxValue Then

        m_Value = MaxValue

    ElseIf m_Value < MinValue Then

        m_Value = MinValue

    End If

End Function

Public Function Round(ByVal DecimalDigits As Long) As CurrencyEx

    Set Round = Me

    On Error Resume Next

    m_Value = Math.Round(m_Value, DecimalDigits)

End Function

Public Function ToArray() As ListEx

    Set ToArray = ITypeConvertible_ToArray

End Function

Public Function ToBoolean() As BooleanEx

    Set ToBoolean = ITypeConvertible_ToBoolean

End Function

Public Function ToByte() As ByteEx

    Set ToByte = ITypeConvertible_ToByte

End Function

Public Function ToDateTime() As DateTimeEx

    Set ToDateTime = ITypeConvertible_ToDateTime

End Function

Public Function ToDecimal() As DecimalEx

    Set ToDecimal = ITypeConvertible_ToDecimal

End Function

Public Function ToDouble() As DoubleEx

    Set ToDouble = ITypeConvertible_ToDouble

End Function

Public Function ToInteger() As IntegerEx

    Set ToInteger = ITypeConvertible_ToInteger

End Function

Public Function ToList() As ListEx

    Set ToList = ITypeConvertible_ToList

End Function

Public Function ToLong() As LongEx

    Set ToLong = ITypeConvertible_ToLong

End Function

Public Function ToSingle() As SingleEx

    Set ToSingle = ITypeConvertible_ToSingle

End Function

Public Function ToString(Optional ByRef Format As String) As StringEx

    Set ToString = ITypeConvertible_ToString(Format)

End Function
