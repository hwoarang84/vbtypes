VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DateTimeEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'INTERFACES

Implements IType
Implements ITypeClonable
Implements ITypeComparable
Implements ITypeConvertible

'CONSTANTS

Private Const CONST_SIZE As Long = 8&

'VARIABLES

Private m_DateMax As Date
Private m_DateMin As Date
Private m_Value As TSYSTEMTIME

'PROPERTIES

Private Property Get IType_Kind() As Long

    IType_Kind = vbDate

End Property

Private Property Get IType_Pointer() As Long

    IType_Pointer = VarPtr(m_Value)

End Property

Private Property Get IType_Size() As Long

    IType_Size = CONST_SIZE

End Property

Private Property Get ITypeComparable_Equals(Value As Variant, ByVal CompareMethod As VbCompareMethod) As Long

    ITypeComparable_Equals = Compare_Date(Me.Value, Value, CompareMethod)

End Property

Public Property Get Day() As Integer

    Day = m_Value.iDay

End Property

Public Property Get Equals(ByRef Value As Variant, Optional ByVal CompareMethod As VbCompareMethod) As Long

    Equals = ITypeComparable_Equals(Value, CompareMethod)

End Property

Public Property Get HasDatePortion() As Boolean

    If m_Value.iYear > 0 Then

        If m_Value.iMonth > 0 Then HasDatePortion = m_Value.iDay > 0

    End If

End Property

Public Property Get HasTimePortion() As Boolean

    If m_Value.iSecond = 0 Then

        If m_Value.iMinute = 0 Then

            If m_Value.iHour = 0 Then Exit Property

        End If

    End If

    HasTimePortion = True

End Property

Public Property Get Hour() As Integer

    Hour = m_Value.iHour

End Property

Public Property Get IsLeap() As Boolean

    If m_Value.iYear > 0 Then IsLeap = DateTime.Month(DateTime.DateSerial(m_Value.iYear, 2, 29)) = 2

End Property

Public Property Get Kind() As VbVarType

    Kind = IType_Kind

End Property

Public Property Get Max() As Date

    If m_DateMax = 0& Then m_DateMax = DateTime.DateSerial(9999, 12, 31) & CONST_CHAR_32 & DateTime.TimeSerial(23, 59, 59)

    Max = m_DateMax

End Property

Public Property Get Millisecond() As Integer

    Millisecond = m_Value.iMilliseconds

End Property

Public Property Get Min() As Date

    If m_DateMin = 0& Then m_DateMin = DateTime.DateSerial(100, 1, 1)

    Min = m_DateMin

End Property

Public Property Get Minute() As Integer

    Minute = m_Value.iMinute

End Property

Public Property Get Month() As Integer

    Month = m_Value.iMonth

End Property

Public Property Get MonthDays() As Long

    If HasDatePortion Then MonthDays = DateTime.Day(DateTime.DateSerial(m_Value.iYear, m_Value.iMonth + 1, 1) + CONST_L_NG)

End Property

Public Property Get Pointer() As Long

    Pointer = IType_Pointer

End Property

Public Property Get Second() As Integer

    Second = m_Value.iSecond

End Property

Public Property Get Size() As Long

    Size = IType_Size

End Property

Public Property Get Value() As Date
Attribute Value.VB_UserMemId = 0
Attribute Value.VB_MemberFlags = "200"

    Dim l_Date As Long
    Dim l_HasTimePortion As Boolean

    l_HasTimePortion = HasTimePortion

    If HasDatePortion Then

        l_Date = DateTime.DateSerial(m_Value.iYear, m_Value.iMonth, m_Value.iDay)

        If l_HasTimePortion Then

            If l_Date <> 0& Then

                Value = CDate(l_Date) & CONST_CHAR_32 & DateTime.TimeSerial(m_Value.iHour, m_Value.iMinute, m_Value.iSecond)

            Else

                Value = DateTime.TimeSerial(m_Value.iHour, m_Value.iMinute, m_Value.iSecond)

            End If

        Else

            If l_Date > 0& Then Value = l_Date

        End If

    Else

        If l_HasTimePortion Then Value = DateTime.TimeSerial(m_Value.iHour, m_Value.iMinute, m_Value.iSecond)

    End If

End Property

Public Property Let Value(ByVal NewValue As Date)

    If NewValue = 0& Then

        Memory_Clear m_Value, Len(m_Value)

    Else

        With m_Value
            .iDay = DateTime.Day(NewValue)
            .iDayOfWeek = DateTime.Weekday(NewValue, vbUseSystemDayOfWeek)
            .iHour = DateTime.Hour(NewValue)
            .iMilliseconds = 0
            .iMinute = DateTime.Minute(NewValue)
            .iMonth = DateTime.Month(NewValue)
            .iSecond = DateTime.Second(NewValue)
            .iYear = DateTime.Year(NewValue)
        End With

    End If

End Property

Public Property Get Weekday() As Integer

    Weekday = m_Value.iDayOfWeek

End Property

Public Property Get Year() As Integer

    Year = m_Value.iYear

End Property

Public Property Get YearDayIndex() As Long

    If HasDatePortion Then YearDayIndex = Abs(DateTime.DateDiff(ChrW$(100&), DateTime.DateSerial(m_Value.iYear, m_Value.iMonth, m_Value.iDay), DateSerial(m_Value.iYear + CONST_L_NG, 12, 31), vbUseSystemDayOfWeek, vbUseSystem))

End Property

'METHODS

Private Function IType_Parse(Value As Variant) As IType

    Set IType_Parse = Me

    If IsObject(Value) Then

        Memory_Clear m_Value, Len(m_Value)

    Else

        Select Case VarType(Value)

            Case vbInteger, vbSingle, vbDate, vbBoolean, vbByte

                Me.Value = Value

            Case vbCurrency, vbDouble, vbDecimal, vbError, vbLong

                If Value > CCur(Min) Then

                    If Value < CCur(Max) Then Me.Value = Value Else Me.Value = Max

                Else

                    Me.Value = Min

                End If

            Case Else

                If IsDate(Value) Then Me.Value = Value Else Memory_Clear m_Value, Len(m_Value)

        End Select

    End If

End Function

Private Function ITypeClonable_Clone() As IType

    Set ITypeClonable_Clone = ITypeConvertible_ToType(New DateTimeEx)

End Function

Private Function ITypeConvertible_ToArray() As IType

    Dim l_List As ListEx

    Set l_List = New ListEx

    Set ITypeConvertible_ToArray = l_List.CreateFrom(Me)

    Set l_List = Nothing

End Function

Private Function ITypeConvertible_ToBoolean() As IType

    Set ITypeConvertible_ToBoolean = ITypeConvertible_ToType(New BooleanEx)

End Function

Private Function ITypeConvertible_ToByte() As IType

    Set ITypeConvertible_ToByte = ITypeConvertible_ToType(New ByteEx)

End Function

Private Function ITypeConvertible_ToCurrency() As IType

    Set ITypeConvertible_ToCurrency = ITypeConvertible_ToType(New CurrencyEx)

End Function

Private Function ITypeConvertible_ToDateTime() As IType

    Set ITypeConvertible_ToDateTime = Me

End Function

Private Function ITypeConvertible_ToDecimal() As IType

    Set ITypeConvertible_ToDecimal = ITypeConvertible_ToType(New DecimalEx)

End Function

Private Function ITypeConvertible_ToDouble() As IType

    Set ITypeConvertible_ToDouble = ITypeConvertible_ToType(New DoubleEx)

End Function

Private Function ITypeConvertible_ToInteger() As IType

    Set ITypeConvertible_ToInteger = ITypeConvertible_ToType(New IntegerEx)

End Function

Private Function ITypeConvertible_ToLong() As IType

    Set ITypeConvertible_ToLong = ITypeConvertible_ToType(New LongEx)

End Function

Private Function ITypeConvertible_ToList() As IType

    Set ITypeConvertible_ToList = ITypeConvertible_ToType(New ListEx)

End Function

Private Function ITypeConvertible_ToSingle() As IType

    Set ITypeConvertible_ToSingle = ITypeConvertible_ToType(New SingleEx)

End Function

Private Function ITypeConvertible_ToString(Optional Format As String) As IType

    Dim l_Result As String

    Set ITypeConvertible_ToString = New StringEx

    If Len(Format) Then l_Result = Strings.Replace(Strings.Format$(Me.Value, Format, vbUseSystemDayOfWeek, vbUseSystem), ChrW$(102&), (m_Value.iMilliseconds), , 1&, vbBinaryCompare) Else l_Result = Me.Value

    ITypeConvertible_ToString.Parse l_Result

End Function

Private Function ITypeConvertible_ToType(Object As IType) As IType

    Set ITypeConvertible_ToType = Object.Parse(Me.Value)

End Function

Public Function AddDays(ByVal Count As Long) As DateTimeEx

    Set AddDays = Me

    AddInterval "d", Count

End Function

Public Function AddHours(ByVal Count As Long) As DateTimeEx

    Set AddHours = Me

    AddInterval "h", Count

End Function

Public Function AddMilliseconds(ByVal Count As Long) As DateTimeEx

    Set AddMilliseconds = Me

    AddNative Count

End Function

Public Function AddMinutes(ByVal Count As Long) As DateTimeEx

    Set AddMinutes = Me

    AddInterval "n", Count

End Function

Public Function AddMonths(ByVal Count As Long) As DateTimeEx

    Set AddMonths = Me

    AddInterval "m", Count

End Function

Public Function AddQuarters(ByVal Count As Long) As DateTimeEx

    Set AddQuarters = Me

    AddInterval "q", Count

End Function

Public Function AddSeconds(ByVal Count As Long) As DateTimeEx

    Set AddSeconds = Me

    AddInterval "s", Count

End Function

Public Function AddYears(ByVal Count As Long) As DateTimeEx

    Set AddYears = Me

    AddInterval "yyyy", Count

End Function

Public Function Clone() As DateTimeEx

    Set Clone = ITypeClonable_Clone

End Function

Public Function DateSerial(ByVal Year As Integer, ByVal Month As Integer, ByVal Day As Integer) As DateTimeEx

    Dim l_Date As Date

    Set DateSerial = Me

    l_Date = DateTime.DateSerial(Year, Month, Day)

    With m_Value
        .iDay = DateTime.Day(l_Date)
        .iDayOfWeek = DateTime.Weekday(l_Date, vbUseSystemDayOfWeek)
        .iMonth = DateTime.Month(l_Date)
        .iYear = DateTime.Year(l_Date)
    End With

End Function

Public Function Now(Optional ByVal UTC As Boolean) As DateTimeEx

    Set Now = Me

    If UTC Then DateTime_GetSystem m_Value Else DateTime_GetLocal m_Value

End Function

Public Function Parse(ByRef Value As Variant) As DateTimeEx

    Set Parse = IType_Parse(Value)

End Function

Public Function TimeSerial(ByVal Hour As Integer, ByVal Minute As Integer, ByVal Second As Integer, Optional ByVal Millisecond As Integer) As DateTimeEx

    Dim l_Date As Date

    Set TimeSerial = Me

    l_Date = DateTime.TimeSerial(Hour, Minute, Second)

    With m_Value
        .iHour = DateTime.Hour(l_Date)
        .iMilliseconds = Millisecond
        .iMinute = DateTime.Minute(l_Date)
        .iSecond = DateTime.Second(l_Date)
    End With

End Function

Public Function ToArray() As ListEx

    Set ToArray = ITypeConvertible_ToArray

End Function

Public Function ToBoolean() As BooleanEx

    Set ToBoolean = ITypeConvertible_ToBoolean

End Function

Public Function ToByte() As ByteEx

    Set ToByte = ITypeConvertible_ToByte

End Function

Public Function ToCurrency() As CurrencyEx

    Set ToCurrency = ITypeConvertible_ToCurrency

End Function

Public Function ToDecimal() As DecimalEx

    Set ToDecimal = ITypeConvertible_ToDecimal

End Function

Public Function ToDouble() As DoubleEx

    Set ToDouble = ITypeConvertible_ToDouble

End Function

Public Function ToInteger() As IntegerEx

    Set ToInteger = ITypeConvertible_ToInteger

End Function

Public Function ToList() As ListEx

    Set ToList = ITypeConvertible_ToList

End Function

Public Function ToLong() As LongEx

    Set ToLong = ITypeConvertible_ToLong

End Function

Public Function ToSingle() As SingleEx

    Set ToSingle = ITypeConvertible_ToSingle

End Function

Public Function ToString(Optional ByRef Format As String) As StringEx

    Set ToString = ITypeConvertible_ToString(Format)

End Function

Private Sub AddInterval(ByRef Interval As String, ByVal Value As Long)

    Dim l_Date As Date

    l_Date = DateTime.DateAdd(Interval, Value, Me.Value)

    With m_Value
        .iDay = DateTime.Day(l_Date)
        .iDayOfWeek = DateTime.Weekday(l_Date, vbUseSystemDayOfWeek)
        .iHour = DateTime.Hour(l_Date)
        .iMilliseconds = 0
        .iMinute = DateTime.Minute(l_Date)
        .iMonth = DateTime.Month(l_Date)
        .iSecond = DateTime.Second(l_Date)
        .iYear = DateTime.Year(l_Date)
    End With

End Sub

Private Sub AddNative(ByVal Value As Long)

    Dim l_FileTime As Currency

    If DateTime_ConvertSystemToFile(m_Value, l_FileTime) Then

        l_FileTime = l_FileTime + Value

        DateTime_ConvertFileToSystem l_FileTime, m_Value

    End If

End Sub
