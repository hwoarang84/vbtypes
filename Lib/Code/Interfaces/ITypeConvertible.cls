VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ITypeConvertible"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'METHODS

Public Function ToArray() As IType
    '
End Function

Public Function ToBoolean() As IType
    '
End Function

Public Function ToByte() As IType
    '
End Function

Public Function ToCurrency() As IType
    '
End Function

Public Function ToDateTime() As IType
    '
End Function

Public Function ToDecimal() As IType
    '
End Function

Public Function ToDouble() As IType
    '
End Function

Public Function ToInteger() As IType
    '
End Function

Public Function ToLong() As IType
    '
End Function

Public Function ToList() As IType
    '
End Function

Public Function ToSingle() As IType
    '
End Function

Public Function ToString(Optional ByRef Format As String) As IType
    '
End Function

Public Function ToType(ByRef Object As IType) As IType
    '
End Function
