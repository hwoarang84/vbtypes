VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ArrayEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'INTERFACES

Implements IType
Implements ITypeClonable
Implements ITypeComparable

'VARIABLES

Private m_SafeArray As TSAFEARRAY
Private m_Sorted As Long

'EVENTS

Private Sub Class_Terminate()

    SafeArray_Destroy m_SafeArray

End Sub

'PROPERTIES

Private Property Get IType_Kind() As Long

    IType_Kind = vbArray + m_SafeArray.lVarType

End Property

Private Property Get IType_Pointer() As Long

    IType_Pointer = m_SafeArray.lPointer

End Property

Private Property Get IType_Size() As Long

    Dim l_Iterator As Long

    If m_SafeArray.lPointer Then

        For l_Iterator = 1& To m_SafeArray.iDims
            IType_Size = IType_Size + SafeArray_GetElementsCount(m_SafeArray, l_Iterator) * m_SafeArray.lElementSize
        Next l_Iterator

    End If

End Property

Private Property Get ITypeComparable_Equals(Value As Variant, ByVal CompareMethod As VbCompareMethod) As Long

    ITypeComparable_Equals = Compare_Any(Me.Value, Value, CompareMethod)

End Property

Public Property Get BoundL(ByVal Dimension As Long) As Long

    If m_SafeArray.lPointer Then BoundL = SafeArray_GetBoundL(m_SafeArray, Dimension)

End Property

Public Property Get BoundU(ByVal Dimension As Long) As Long

    If m_SafeArray.lPointer Then BoundU = SafeArray_GetBoundU(m_SafeArray, Dimension)

End Property

Public Property Get Data() As Long

    Data = m_SafeArray.lData

End Property

Public Property Get Dimensions() As Long

    Dimensions = m_SafeArray.iDims

End Property

Public Property Get Elements(ByVal Dimension As Long) As Long

    If m_SafeArray.lPointer Then Elements = SafeArray_GetElementsCount(m_SafeArray, Dimension)

End Property

Public Property Get ElementSize() As Long

    ElementSize = m_SafeArray.lElementSize

End Property

Public Property Get ElementType() As VbVarType

    ElementType = m_SafeArray.lVarType

End Property

Public Property Get Equals(ByRef Value As Variant, Optional ByVal CompareMethod As VbCompareMethod) As Long

    Equals = ITypeComparable_Equals(Value, CompareMethod)

End Property

Public Property Get Exists() As Boolean

    If m_SafeArray.lPointer Then

        If m_SafeArray.lData > 0& Then Exists = m_SafeArray.iDims > 0

    End If

End Property

Public Property Get Item(ParamArray Indexes()) As Variant

    Dim l_Indexes() As Long
    Dim l_Iterator As Long
    Dim l_Result As ENUM_OLE_STATUS

    On Error GoTo ErrorHandler

    If m_SafeArray.lPointer Then

        l_Iterator = UBound(Indexes)

        If l_Iterator > CONST_L_NG And l_Iterator = m_SafeArray.iDims + CONST_L_NG Then

            l_Result = E_INVALIDARG

            VariantInit Item, m_SafeArray.lVarType

            If m_SafeArray.iDims > 1 Then

                ReDim l_Indexes(l_Iterator)

                For l_Iterator = 0& To l_Iterator
                    l_Indexes(l_Iterator) = Indexes(l_Iterator)
                Next l_Iterator

                l_Result = SafeArray_GetElement(m_SafeArray, l_Indexes(0), Item)

                Erase l_Indexes

            Else

                l_Result = ItemGetInternal(Indexes(0), Item)

            End If

        End If

    End If

ErrorHandler:

    If l_Result <> S_OK Then VariantZero Item

End Property

Public Property Let Item(ParamArray Indexes(), ByRef NewValue As Variant)

    Dim l_Indexes() As Long
    Dim l_Iterator As Long
    Dim l_Result As ENUM_OLE_STATUS

    On Error GoTo ErrorHandler

    If m_SafeArray.lPointer Then

        l_Iterator = UBound(Indexes)

        If l_Iterator > CONST_L_NG And l_Iterator = m_SafeArray.iDims + CONST_L_NG Then

            If m_SafeArray.iDims > 1 Then

                ReDim l_Indexes(l_Iterator)

                For l_Iterator = 0& To l_Iterator
                    l_Indexes(l_Iterator) = Indexes(l_Iterator)
                Next l_Iterator

                l_Result = SafeArray_SetElement(m_SafeArray, l_Indexes(0), NewValue)

                Erase l_Indexes

            Else

                l_Result = ItemSetInternal(Indexes(0), NewValue)

            End If

            If l_Result = S_OK Then m_Sorted = 0&

        End If

    End If

ErrorHandler:

End Property

Public Property Get ItemExists(ParamArray Indexes()) As Boolean

    Dim l_Iterator As Long

    On Error GoTo ErrorHandler

    If m_SafeArray.lPointer Then

        l_Iterator = UBound(Indexes)

        If l_Iterator > CONST_L_NG And l_Iterator = m_SafeArray.iDims + CONST_L_NG Then

            For l_Iterator = 0& To l_Iterator

                If Not ItemExistsInternal(Indexes(l_Iterator), l_Iterator + 1&) Then Exit Property

            Next l_Iterator

            ItemExists = True

        End If

    End If

ErrorHandler:

End Property

Public Property Get Kind() As VbVarType

    Kind = IType_Kind

End Property

Public Property Get Pointer() As Long

    Pointer = IType_Pointer

End Property

Public Property Get Size() As Long

    Size = IType_Size

End Property

Friend Property Get Sorted() As Long

    Sorted = m_Sorted

End Property

Friend Property Let Sorted(ByVal NewValue As Long)

    m_Sorted = NewValue

End Property

Public Property Get Value() As Variant
Attribute Value.VB_UserMemId = 0
Attribute Value.VB_MemberFlags = "200"

    If m_SafeArray.lPointer Then SafeArray_Copy m_SafeArray, Value

End Property

Public Property Let Value(ByRef NewValue As Variant)

    ParseInternal NewValue

End Property

'METHODS

Private Function IType_Parse(Value As Variant) As IType

    Set IType_Parse = Me

    If Not ParseInternal(Value) Then Add Value

End Function

Private Function ITypeClonable_Clone() As IType

    Set ITypeClonable_Clone = New ArrayEx

    If m_SafeArray.lPointer Then ITypeClonable_Clone.Parse Me.Value

End Function

Public Function Add(ByRef Item As Variant) As ArrayEx

    Dim l_Bounds() As Long
    Dim l_Long As Long
    Dim l_Result As ENUM_OLE_STATUS

    Set Add = Me

    If m_SafeArray.lPointer Then

        l_Long = m_SafeArray.uBounds(0).lElements

        If SafeArray_Redim(m_SafeArray, l_Long + 1&) = S_OK Then

            If m_SafeArray.iDims > 1 Then

                GetIndexesFromSelf l_Bounds

                l_Result = SafeArray_SetElement(m_SafeArray, l_Bounds(0), Item)

                Erase l_Bounds

            Else

                l_Result = SafeArray_SetElement(m_SafeArray, SafeArray_GetBoundU(m_SafeArray, 1&), Item)

            End If

            If l_Result <> S_OK Then SafeArray_Redim m_SafeArray, l_Long

            m_Sorted = 0&

        End If

    Else

        If IsObject(Item) Then

            l_Long = vbObject

        Else

            l_Long = VarType(Item)

            If (l_Long And vbArray) = vbArray Or l_Long = vbEmpty Or l_Long = vbNull Then l_Long = vbVariant

        End If

        If SafeArray_CreateVector(m_SafeArray, l_Long, 1&) = S_OK Then

            If SafeArray_SetElement(m_SafeArray, 0&, Item) <> S_OK Then SafeArray_Redim m_SafeArray, 0&

        End If

    End If

End Function

Public Function AddRange(ByRef Range As Variant) As ArrayEx

    Dim l_Array As TSAFEARRAY
    Dim l_Elements As Long
    Dim l_Indexes() As Long
    Dim l_Iterator As Long
    Dim l_UBound As Long

    Set AddRange = Me

    SafeArray_FillInfo l_Array, VarPtr(Range)

    If l_Array.lPointer > 0& And l_Array.iDims = 1& Then

        If m_SafeArray.lPointer Then

            l_UBound = GetIndexesFromSelf(l_Indexes)
            l_Elements = m_SafeArray.uBounds(0).lElements

            If SafeArray_Redim(m_SafeArray, l_Elements + l_Array.uBounds(0).lElements) = S_OK Then

                For l_Iterator = SafeArray_GetBoundL(l_Array, m_SafeArray.iDims) To SafeArray_GetBoundU(l_Array, m_SafeArray.iDims)

                    l_Indexes(l_UBound) = l_Indexes(l_UBound) + 1&

                    If SafeArray_SetElement(m_SafeArray, l_Indexes(0), Range(l_Iterator)) <> S_OK Then

                        SafeArray_Redim m_SafeArray, l_Elements

                        Exit For

                    End If

                Next l_Iterator

                m_Sorted = 0&

            End If

            Erase l_Indexes

        Else

            If SafeArray_CreateVector(m_SafeArray, l_Array.lVarType, l_Array.uBounds(0).lElements) = S_OK Then

                If SafeArray_CopyData(l_Array, m_SafeArray) <> S_OK Then SafeArray_Redim m_SafeArray, 0&

            End If

        End If

    Else

        Add Range

    End If

End Function

Public Function Clone() As ArrayEx

    Set Clone = ITypeClonable_Clone

End Function

Public Function Create(ByVal ArrayType As VbVarType, ParamArray Bounds()) As ArrayEx

    Dim l_Indexes() As TSAFEARRAYBOUND
    Dim l_Iterator As Long
    Dim l_UBound As Long

    Set Create = Me

    On Error GoTo ErrorHandler

    l_UBound = UBound(Bounds)

    ReDim l_Indexes(l_UBound)

    For l_Iterator = 0& To l_UBound
        l_Indexes(l_Iterator).lLowest = Bounds(l_Iterator)(0)
        l_Indexes(l_Iterator).lElements = Bounds(l_Iterator)(1) - l_Indexes(l_Iterator).lLowest + 1&
    Next l_Iterator

    If SafeArray_Destroy(m_SafeArray) = S_OK Then SafeArray_Create m_SafeArray, ArrayType, l_UBound + 1&, l_Indexes

ErrorHandler:

    Erase l_Indexes

End Function

Public Function Destroy() As ArrayEx

    Set Destroy = Me

    m_Sorted = 0&

    Class_Terminate

End Function

Public Function Parse(ByRef Value As Variant) As ArrayEx

    Set Parse = IType_Parse(Value)

End Function

Public Function Resize(ByVal ElementsCount As Long) As ArrayEx

    Dim l_Elements As Long
    Dim l_ShouldResetSorted As Boolean

    Set Resize = Me

    If ElementsCount >= 0& Then l_Elements = ElementsCount

    If m_SafeArray.lPointer Then

        l_ShouldResetSorted = l_Elements > SafeArray_GetElementsCount(m_SafeArray, m_SafeArray.iDims)

        If SafeArray_Redim(m_SafeArray, l_Elements) = S_OK And l_ShouldResetSorted Then m_Sorted = 0&

    Else

        SafeArray_CreateVector m_SafeArray, vbVariant, l_Elements

    End If

End Function

Public Function ToList(ByRef StartIndexes As Variant, ByRef EndIndexes As Variant) As ListEx

    Dim l_Index As Long
    Dim l_Indexes() As Long
    Dim l_UBound As Long

    Set ToList = New ListEx

    On Error GoTo ErrorHandler

    If m_SafeArray.lPointer Then

        l_UBound = UBound(StartIndexes)

        If l_UBound = m_SafeArray.iDims + CONST_L_NG And l_UBound = UBound(EndIndexes) Then

            If m_SafeArray.iDims > 1 Then l_Index = GetIndexesFromArray(StartIndexes, l_Indexes, l_UBound) Else l_Index = CONST_L_NG

            ToListInternal ToList, StartIndexes(l_UBound), EndIndexes(l_UBound), l_Indexes, l_Index

        End If

    End If

ErrorHandler:

    Erase l_Indexes

End Function

Public Function ToString(ByRef StartIndexes As Variant, ByRef EndIndexes As Variant, Optional ByRef Delimeter As String) As StringEx

    Dim l_Index As Long
    Dim l_Indexes() As Long
    Dim l_UBound As Long

    Set ToString = New StringEx

    On Error GoTo ErrorHandler

    If m_SafeArray.lPointer > 0& And m_SafeArray.lVarType <> vbObject Then

        l_UBound = UBound(StartIndexes)

        If l_UBound = m_SafeArray.iDims + CONST_L_NG And l_UBound = UBound(EndIndexes) Then

            If m_SafeArray.iDims > 1 Then l_Index = GetIndexesFromArray(StartIndexes, l_Indexes, l_UBound) Else l_Index = CONST_L_NG

            ToStringInternal ToString, StartIndexes(l_UBound), EndIndexes(l_UBound), l_Indexes, l_Index, Delimeter

        End If

    End If

ErrorHandler:

    Erase l_Indexes

End Function

Friend Function ItemExistsInternal(ByVal lIndex As Long, ByVal lDim As Long) As Boolean

    Dim l_LBound As Long

    l_LBound = SafeArray_GetBoundL(m_SafeArray, lDim)

    If lIndex > l_LBound Then

        ItemExistsInternal = lIndex <= SafeArray_GetBoundU(m_SafeArray, lDim)

    ElseIf lIndex = l_LBound Then

        ItemExistsInternal = True

    End If

End Function

Friend Function ItemGetInternal(ByVal lIndex As Long, ByRef vItem As Variant) As ENUM_OLE_STATUS

    ItemGetInternal = SafeArray_GetElement(m_SafeArray, lIndex, vItem)

End Function

Friend Function ItemSetInternal(ByVal lIndex As Long, ByRef vItem As Variant) As ENUM_OLE_STATUS

    ItemSetInternal = SafeArray_SetElement(m_SafeArray, lIndex, vItem)

End Function

Friend Sub ToListInternal(ByRef oObject As ListEx, ByVal StartIndex As Long, ByVal EndIndex As Long, ByRef Indexes() As Long, ByVal IndexesUpperBound As Long)

    Dim l_Iterator As Long
    Dim l_Result As ENUM_OLE_STATUS
    Dim l_Value As Variant

    If IndexesUpperBound = CONST_L_NG Then

        If StartIndex = SafeArray_GetBoundL(m_SafeArray, 1&) Then

            If EndIndex = SafeArray_GetBoundU(m_SafeArray, 1&) Then

                oObject.AddRange Me.Value

                Exit Sub

            End If

        End If

    End If

    VariantInit l_Value, m_SafeArray.lVarType

    For l_Iterator = StartIndex To EndIndex

        If IndexesUpperBound > CONST_L_NG Then

            Indexes(IndexesUpperBound) = l_Iterator

            l_Result = SafeArray_GetElement(m_SafeArray, Indexes(0), l_Value)

        Else

            l_Result = SafeArray_GetElement(m_SafeArray, l_Iterator, l_Value)

        End If

        If l_Result = S_OK Then oObject.Add l_Value

    Next l_Iterator

    VariantZero l_Value

End Sub

Friend Sub ToStringInternal(ByRef oObject As StringEx, ByVal StartIndex As Long, ByVal EndIndex As Long, ByRef Indexes() As Long, ByVal IndexesUpperBound As Long, ByRef Delimeter As String)

    Dim l_Iterator As Long
    Dim l_Result As ENUM_OLE_STATUS
    Dim l_Value As Variant

    VariantInit l_Value, m_SafeArray.lVarType

    For l_Iterator = StartIndex To EndIndex

        If IndexesUpperBound > CONST_L_NG Then

            Indexes(IndexesUpperBound) = l_Iterator

            l_Result = SafeArray_GetElement(m_SafeArray, Indexes(0), l_Value)

        Else

            l_Result = SafeArray_GetElement(m_SafeArray, l_Iterator, l_Value)

        End If

        If l_Result = S_OK Then oObject.ConcatPointer StrPtr(l_Value & Delimeter)

    Next l_Iterator

    oObject.Left oObject.Length - Len(Delimeter)

    VariantZero l_Value

End Sub

Private Function GetIndexesFromArray(ByRef aInput As Variant, ByRef aOutput() As Long, ByVal lUbound As Long) As Long

    Dim l_Iterator As Long

    ReDim aOutput(lUbound)

    For l_Iterator = 0& To lUbound
        aOutput(l_Iterator) = aInput(l_Iterator)
    Next l_Iterator

    GetIndexesFromArray = lUbound

End Function

Private Function GetIndexesFromSelf(ByRef aOutput() As Long) As Long

    Dim l_Iterator As Long
    Dim l_UBound As Long

    l_UBound = m_SafeArray.iDims + CONST_L_NG

    ReDim aOutput(l_UBound)

    For l_Iterator = 0& To l_UBound
        aOutput(l_Iterator) = SafeArray_GetBoundU(m_SafeArray, l_Iterator + 1&)
    Next l_Iterator

    GetIndexesFromSelf = l_UBound

End Function

Private Function ParseInternal(ByRef Value As Variant) As Boolean

    Dim l_Array As TSAFEARRAY
    Dim l_Bounds() As TSAFEARRAYBOUND
    Dim l_Iterator As Long
    Dim l_Result As ENUM_OLE_STATUS

    ParseInternal = True

    If SafeArray_Destroy(m_SafeArray) = S_OK Then

        SafeArray_FillInfo l_Array, VarPtr(Value)

        If l_Array.lPointer Then

            ReDim l_Bounds(l_Array.iDims + CONST_L_NG)

            For l_Iterator = 0& To l_Array.iDims + CONST_L_NG
                l_Bounds(l_Array.iDims - l_Iterator + CONST_L_NG) = l_Array.uBounds(l_Iterator)
            Next l_Iterator

            l_Result = SafeArray_Create(m_SafeArray, l_Array.lVarType, l_Array.iDims, l_Bounds)

            Erase l_Bounds

            If l_Result = S_OK Then SafeArray_CopyData l_Array, m_SafeArray

        Else

            ParseInternal = False

        End If

    End If

End Function
