VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StringEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'INTERFACES

Implements IType
Implements ITypeClonable
Implements ITypeComparable
Implements ITypeConvertible

'CONSTANTS

Private Const CONST_INITIAL_BUFFER_SIZE As Long = 128&
Private Const CONST_SIZE As Long = 4&

'VARIABLES

Private m_CryptContext As TCRYPTOGRAPHYCONTEXT
Private m_Value As String
Private m_ValueLen As Long
Private m_ValueLenReal As Long
Private m_ValuePtr As Long

'EVENTS

Private Sub Class_Initialize()

    m_ValueLenReal = CONST_INITIAL_BUFFER_SIZE

End Sub

Private Sub Class_Terminate()

    Cryptography_Destroy m_CryptContext

End Sub

'PROPERTIES

Private Property Get IType_Kind() As Long

    IType_Kind = vbString

End Property

Private Property Get IType_Pointer() As Long

    IType_Pointer = m_ValuePtr

End Property

Private Property Get IType_Size() As Long

    IType_Size = CONST_SIZE

End Property

Private Property Get ITypeComparable_Equals(Value As Variant, ByVal CompareMethod As VbCompareMethod) As Long

    ITypeComparable_Equals = Compare_StringByPointer(m_ValuePtr, m_ValueLen, Value, CompareMethod)

End Property

Public Property Get Asc(Optional ByVal Index As Long) As Long

    If m_ValueLen Then

        If Index >= 0& And Index <= m_ValueLen Then Memory_Copy Asc, ByVal m_ValuePtr + ((Index + (Index > 0&)) * 2&), 2&

    End If

End Property

Public Property Get Capacity() As Long

    Capacity = m_ValueLenReal

End Property

Public Property Get Contains(ByRef SearchString As String, Optional ByVal CompareMethod As VbCompareMethod) As Boolean

    Contains = IndexOf(SearchString, , , CompareMethod)

End Property

Public Property Get Equals(ByRef Value As Variant, Optional ByVal CompareMethod As VbCompareMethod) As Long

    Equals = ITypeComparable_Equals(Value, CompareMethod)

End Property

Public Property Get HashCode() As Long

    If CryptCreate Then HashCode = m_CryptContext.lHash

End Property

Public Property Get IndexOf(ByRef SearchString As String, Optional ByVal Start As Long, Optional ByVal Reverse As Boolean, Optional ByVal CompareMethod As VbCompareMethod) As Long

    Dim l_Index As Long
    Dim l_Length As Long

    l_Length = Len(SearchString)

    If m_ValueLen > 0& And l_Length > 0& Then

        If Reverse Then

            If Start > 0& And Start < m_ValueLen Then l_Index = Start Else l_Index = m_ValueLen

            IndexOf = InStrRev(m_Value, SearchString, l_Index, CompareMethod)

        Else

            If Start > 0& Then l_Index = Start Else l_Index = 1&

            l_Index = InStr(l_Index, m_Value, SearchString, CompareMethod)

            If l_Index <= m_ValueLen - l_Length Then IndexOf = l_Index

        End If

    End If

End Property

Public Property Get IsEmptyOrWhiteSpace() As Boolean

    Dim l_Index As Long

    If m_ValueLen Then

        Do While l_Index < m_ValueLen And InStr(l_Index + 1&, m_Value, CONST_CHAR_32, vbBinaryCompare) = (l_Index + 1&)
            l_Index = l_Index + 1&
        Loop

    End If

    IsEmptyOrWhiteSpace = l_Index = m_ValueLen

End Property

Public Property Get Kind() As VbVarType

    Kind = IType_Kind

End Property

Public Property Get Length() As Long

    Length = m_ValueLen

End Property

Public Property Get Pointer() As Long

    Pointer = IType_Pointer

End Property

Public Property Get Size() As Long

    Size = IType_Size

End Property

Public Property Get Value() As String
Attribute Value.VB_UserMemId = 0
Attribute Value.VB_MemberFlags = "200"

    If m_ValueLen Then Value = VBA.Left$(m_Value, m_ValueLen)

End Property

Public Property Let Value(ByRef NewValue As String)

    Dim l_Length As Long

    l_Length = Len(NewValue)

    If l_Length Then

        BufferRedim l_Length, True

        Memory_Copy ByVal m_ValuePtr, ByVal StrPtr(NewValue), m_ValueLen * 2&

    Else

        m_ValueLen = 0&

    End If

End Property

'METHODS

Private Function IType_Parse(Value As Variant) As IType

    Dim l_Array As TSAFEARRAY
    Dim l_Bom As Long
    Dim l_Char As Integer
    Dim l_Length As Long
    Dim l_Offset As Long

    Set IType_Parse = Me

    If IsObject(Value) Then

        m_ValueLen = 0&

    Else

        Select Case VarType(Value)

            Case vbInteger To vbString, vbError, vbBoolean, vbDecimal, vbByte

                Me.Value = Value

            Case vbArray + vbByte

                SafeArray_FillInfo l_Array, VarPtr(Value)

                If l_Array.iDims = 1 Then

                    l_Length = l_Array.uBounds(0).lElements

                    If l_Length Mod 2& Then 'odd bytes count (ANSI, UTF-8)

                        If l_Length >= 3& Then

                            If Value(0) = 239 And Value(1) = 187 And Value(2) = 191 Then l_Offset = 3& 'check for UTF-8 byte order mark

                            If ConvertMultiByteToWideChar(l_Array.lData, l_Length, (l_Offset > 0&) * CONST_L_NG, l_Offset) Then Exit Function '7-bit ANSI or UTF-8

                        End If

                        BufferRedim l_Length, True
                        Memory_Copy ByVal m_ValuePtr, ByVal l_Array.lData, l_Length 'copy bytes as 8-bit ANSI

                    ElseIf l_Length > 0& Then 'even bytes count (ANSI, UTF-8, UTF-16 BE/LE)

                        Select Case Value(0)

                            Case 255

                                If Value(1) = 254 Then l_Bom = 2& 'UTF-16 Little Endian

                            Case 254

                                If Value(1) = 255 Then 'UTF-16 Big Endian

                                    For l_Offset = 2& To l_Length + CONST_L_NG Step 2& 'convert to Little Endian order
                                        Memory_Get2 ByVal l_Array.lData + l_Offset, l_Char
                                        l_Char = (((l_Char And &HFF00) \ &H100) And &HFF) Or (l_Char And &H7F) * &H100 Or (l_Char And &H80) * &HFF00 'swap integer low & high words
                                        Memory_Put2 ByVal l_Array.lData + l_Offset, ByVal l_Char
                                    Next l_Offset

                                    l_Bom = 2&

                                End If

                            Case 239

                                If l_Length >= 3& Then

                                    If Value(1) = 187 And Value(2) = 191 Then 'UTF-8

                                        If ConvertMultiByteToWideChar(l_Array.lData, l_Length, 1&, 3&) Then Exit Function

                                    End If

                                End If

                        End Select

                        If l_Bom = 0& Then 'no BOM

                            If Not String_TestUnicode(l_Array.lData, l_Length, IS_TEXT_UNICODE_ASCII16 Or IS_TEXT_UNICODE_STATISTICS Or IS_TEXT_UNICODE_CONTROLS) Then

                                If Not ConvertMultiByteToWideChar(l_Array.lData, l_Length, 0&, 0&) Then '7-bit ANSI or UTF-8

                                    BufferRedim l_Length, True
                                    Memory_Copy ByVal m_Value, ByVal l_Array.lData, l_Length 'copy bytes as 8-bit ANSI

                                End If

                                Exit Function

                            End If

                        End If

                        BufferRedim (l_Length - l_Bom) \ 2&, True
                        Memory_Copy ByVal m_ValuePtr, ByVal l_Array.lData + l_Bom, l_Length - l_Bom 'copy bytes as Unicode

                    End If

                End If

            Case Else

                m_ValueLen = 0&

        End Select

    End If

End Function

Private Function ITypeClonable_Clone() As IType

    Set ITypeClonable_Clone = ITypeConvertible_ToType(New StringEx)

End Function

Private Function ITypeConvertible_ToArray() As IType

    Dim l_Length As Long
    Dim l_List As ListEx

    Set l_List = New ListEx

    If m_ValueLen Then

        l_Length = m_ValueLen * 2&

        l_List.Create vbByte, l_Length

        Memory_Copy ByVal l_List.Data, ByVal m_ValuePtr, l_Length

    End If

    Set ITypeConvertible_ToArray = l_List

    Set l_List = Nothing

End Function

Private Function ITypeConvertible_ToBoolean() As IType

    Set ITypeConvertible_ToBoolean = ITypeConvertible_ToType(New BooleanEx)

End Function

Private Function ITypeConvertible_ToByte() As IType

    Set ITypeConvertible_ToByte = ITypeConvertible_ToType(New ByteEx)

End Function

Private Function ITypeConvertible_ToCurrency() As IType

    Set ITypeConvertible_ToCurrency = ITypeConvertible_ToType(New CurrencyEx)

End Function

Private Function ITypeConvertible_ToDateTime() As IType

    Set ITypeConvertible_ToDateTime = ITypeConvertible_ToType(New DateTimeEx)

End Function

Private Function ITypeConvertible_ToDecimal() As IType

    Set ITypeConvertible_ToDecimal = ITypeConvertible_ToType(New DecimalEx)

End Function

Private Function ITypeConvertible_ToDouble() As IType

    Set ITypeConvertible_ToDouble = ITypeConvertible_ToType(New DoubleEx)

End Function

Private Function ITypeConvertible_ToInteger() As IType

    Set ITypeConvertible_ToInteger = ITypeConvertible_ToType(New IntegerEx)

End Function

Private Function ITypeConvertible_ToLong() As IType

    Set ITypeConvertible_ToLong = ITypeConvertible_ToType(New LongEx)

End Function

Private Function ITypeConvertible_ToList() As IType

    Set ITypeConvertible_ToList = ITypeConvertible_ToType(New ListEx)

End Function

Private Function ITypeConvertible_ToSingle() As IType

    Set ITypeConvertible_ToSingle = ITypeConvertible_ToType(New SingleEx)

End Function

Private Function ITypeConvertible_ToString(Optional Format As String) As IType

    Set ITypeConvertible_ToString = Me

End Function

Private Function ITypeConvertible_ToType(Object As IType) As IType

    If m_ValueLen Then Set ITypeConvertible_ToType = Object.Parse(VBA.Left$(m_Value, m_ValueLen)) Else Set ITypeConvertible_ToType = Object

End Function

Public Function Clone() As StringEx

    Set Clone = ITypeClonable_Clone

End Function

Public Function Concat(ByRef ConcatString As String) As StringEx

    Set Concat = ConcatPointer(StrPtr(ConcatString))

End Function

Public Function ConcatPointer(ByVal ConcatStringPointer As Long) As StringEx

    Dim l_Length As Long

    Set ConcatPointer = Me

    If ConcatStringPointer Then

        l_Length = String_GetCharsCount(ConcatStringPointer)

        If l_Length Then

            BufferRedim l_Length, False

            Memory_Copy ByVal m_ValuePtr + ((m_ValueLen - l_Length) * 2&), ByVal ConcatStringPointer, l_Length * 2&

        End If

    End If

End Function

Public Function Decrypt() As StringEx

    Set Decrypt = Me

    If CryptCreate And m_ValueLen Then Cryptography_Decrypt m_CryptContext.lKey, m_ValuePtr, m_ValueLenReal * 2&

End Function

Public Function Duplicate(ByVal Count As Long) As StringEx

    Dim l_Iterator As Long
    Dim l_Length As Long

    Set Duplicate = Me

    If m_ValueLen > 0& And Count > 0& Then

        l_Length = m_ValueLen

        BufferRedim l_Length * Count, False

        For l_Iterator = 1& To Count
            Memory_Copy ByVal m_ValuePtr + ((l_Length * l_Iterator) * 2&), ByVal m_ValuePtr, l_Length * 2&
        Next l_Iterator

    End If

End Function

Public Function Encrypt() As StringEx

    Set Encrypt = Me

    If CryptCreate And m_ValueLen Then Cryptography_Encrypt m_CryptContext.lKey, m_ValuePtr, m_ValueLen * 2&, m_ValueLenReal * 2&

End Function

Public Function Insert(ByVal Index As Long, ByRef InsertString As String) As StringEx

    Dim l_Length As Long

    Set Insert = Me

    l_Length = Len(InsertString)

    If l_Length > 0& And Index >= 0& And Index <= m_ValueLen Then

        BufferRedim l_Length, False

        Memory_Copy ByVal m_ValuePtr + (Index * 2&) + l_Length + l_Length, ByVal m_ValuePtr + (Index * 2&), (m_ValueLen - (Index + l_Length)) * 2&
        Memory_Copy ByVal m_ValuePtr + (Index * 2&), ByVal StrPtr(InsertString), l_Length + l_Length

    End If

End Function

Public Function Left(ByVal Length As Long) As StringEx

    Set Left = Me

    If Length >= 0& And Length < m_ValueLen Then m_ValueLen = Length

End Function

Public Function Lower() As StringEx

    Set Lower = Me

    If m_ValueLen Then String_ConvertToLower m_ValuePtr, m_ValueLen

End Function

Public Function Mid(ByVal Start As Long, Optional ByVal Length As Long) As StringEx

    Set Mid = Me

    If m_ValueLen > 0& And Start > 0& And Start <= m_ValueLen Then

        If Length > 0& And Length <= (m_ValueLen - Start) Then m_ValueLen = Length Else m_ValueLen = m_ValueLen - Start + 1&

        Memory_Copy ByVal m_ValuePtr, ByVal m_ValuePtr + ((Start + CONST_L_NG) * 2&), m_ValueLen * 2&

    End If

End Function

Public Function Numeric() As StringEx

    Set Numeric = Me

    Me.Value = ConvertToNumeric(m_ValuePtr, m_ValueLen)

End Function

Public Function PadLeft(ByVal Width As Long) As StringEx

    Dim l_Length As Long

    Set PadLeft = Me

    If Width > m_ValueLen Then

        l_Length = Width - m_ValueLen

        BufferRedim l_Length, False

        Memory_Copy ByVal m_ValuePtr + l_Length + l_Length, ByVal m_ValuePtr, (m_ValueLen - l_Length) * 2&
        Memory_Copy ByVal m_ValuePtr, ByVal StrPtr(Space$(l_Length)), l_Length + l_Length

    End If

End Function

Public Function PadRight(ByVal Width As Long) As StringEx

    Dim l_Length As Long

    Set PadRight = Me

    If Width > m_ValueLen Then

        l_Length = Width - m_ValueLen

        BufferRedim l_Length, False

        Memory_Copy ByVal m_ValuePtr + ((m_ValueLen - l_Length) * 2&), ByVal StrPtr(Space$(l_Length)), l_Length + l_Length

    End If

End Function

Public Function Parse(ByRef Value As Variant) As StringEx

    Set Parse = IType_Parse(Value)

End Function

Public Function Remove(ByVal Index As Long, ByVal Length As Long) As StringEx

    Dim l_Length As Long

    Set Remove = Me

    If m_ValueLen > 0& And Index > 0& And Index <= m_ValueLen And Length > 0& Then

        l_Length = Length - (Length - (m_ValueLen - Index + 1&))

        If Length <= l_Length Then l_Length = Length

        If l_Length < m_ValueLen Then

            Memory_Copy ByVal m_ValuePtr + ((Index + CONST_L_NG) * 2&), ByVal m_ValuePtr + ((Index + CONST_L_NG) * 2&) + (l_Length * 2&), (m_ValueLen - (Index + l_Length) + 1&) * 2&

            m_ValueLen = m_ValueLen - l_Length

        Else

            m_ValueLen = 0&

        End If

    End If

End Function

Public Function Replace(ByRef SearchString As String, ByRef ReplaceString As String, Optional ByVal Start As Long = 1&, Optional ByVal Count As Long = CONST_L_NG, Optional ByVal CompareMethod As VbCompareMethod) As StringEx

    Set Replace = Me

    If m_ValueLen Then Value = VBA.Replace(VBA.Left$(m_Value, m_ValueLen), SearchString, ReplaceString, Start, Count, CompareMethod)

End Function

Public Function Right(ByVal Length As Long) As StringEx

    Set Right = Me

    If Length >= 0& And Length < m_ValueLen Then

        Memory_Copy ByVal m_ValuePtr, ByVal m_ValuePtr + ((m_ValueLen - Length) * 2&), Length + Length

        m_ValueLen = Length

    End If

End Function

Public Function Split(Optional ByRef Delimeter As String, Optional ByVal Limit As Long, Optional ByVal CompareMethod As VbCompareMethod, Optional ByVal ArrayType As VbVarType = vbString) As ListEx

    Dim l_Buffer As String
    Dim l_BufferPtr As Long
    Dim l_Iterator As Long
    Dim l_Length As Long
    Dim l_Position As Long
    Dim l_PositionLast As Long

    Set Split = New ListEx

    On Error Resume Next

    If m_ValueLen > 0& Then

        Split.Create ArrayType, 0&

        If Len(Delimeter) > 0& Then

            l_Buffer = Space$(m_ValueLen)
            l_BufferPtr = StrPtr(l_Buffer)

            Do
                l_Position = InStr(l_Position + 1&, m_Value, Delimeter, CompareMethod)

                If l_Position = 0& Or l_Position > m_ValueLen Then l_Position = m_ValueLen + 1&

                l_Length = l_Position - l_PositionLast + CONST_L_NG

                Memory_Copy ByVal l_BufferPtr, ByVal m_ValuePtr + l_PositionLast + l_PositionLast, l_Length + l_Length

                Split.Add VBA.Left$(l_Buffer, l_Length)

                l_Iterator = l_Iterator + 1&
                l_PositionLast = l_Position

            Loop While (Limit = 0& Or (Limit > 0& And l_Iterator < Limit)) And l_PositionLast < m_ValueLen

        Else

            Split.Add VBA.Left$(m_Value, m_ValueLen)

        End If

    End If

End Function

Public Function ToArray(Optional ByVal AsANSI As Boolean) As ListEx

    Dim l_List As ListEx

    If AsANSI Then

        Set l_List = New ListEx

        l_List.Create vbByte, m_ValueLen

        If m_ValueLen Then Memory_Copy ByVal l_List.Data, ByVal m_Value, m_ValueLen

        Set ToArray = l_List

        Set l_List = Nothing

    Else

        Set ToArray = ITypeConvertible_ToArray

    End If

End Function

Public Function ToBoolean() As BooleanEx

    Set ToBoolean = ITypeConvertible_ToBoolean

End Function

Public Function ToByte() As ByteEx

    Set ToByte = ITypeConvertible_ToByte

End Function

Public Function ToCurrency() As CurrencyEx

    Set ToCurrency = ITypeConvertible_ToCurrency

End Function

Public Function ToDateTime() As DateTimeEx

    Set ToDateTime = ITypeConvertible_ToDateTime

End Function

Public Function ToDecimal() As DecimalEx

    Set ToDecimal = ITypeConvertible_ToDecimal

End Function

Public Function ToDouble() As DoubleEx

    Set ToDouble = ITypeConvertible_ToDouble

End Function

Public Function ToInteger() As IntegerEx

    Set ToInteger = ITypeConvertible_ToInteger

End Function

Public Function ToList() As ListEx

    Set ToList = ITypeConvertible_ToList

End Function

Public Function ToLong() As LongEx

    Set ToLong = ITypeConvertible_ToLong

End Function

Public Function ToSingle() As SingleEx

    Set ToSingle = ITypeConvertible_ToSingle

End Function

Public Function TrimL() As StringEx

    Dim l_Position As Long

    Set TrimL = Me

    If m_ValueLen Then

        Do While l_Position < m_ValueLen And InStr(l_Position + 1&, m_Value, CONST_CHAR_32, vbBinaryCompare) = (l_Position + 1&)
            l_Position = l_Position + 1&
        Loop

        If l_Position Then

            m_ValueLen = m_ValueLen - l_Position

            If m_ValueLen Then Memory_Copy ByVal m_ValuePtr, ByVal m_ValuePtr + (l_Position * 2&), m_ValueLen * 2&

        End If

    End If

End Function

Public Function TrimNull() As StringEx

    Dim l_Position As Long

    Set TrimNull = Me

    If m_ValueLen Then

        l_Position = InStr(1&, m_Value, vbNullChar, vbBinaryCompare)

        If l_Position > 0& And l_Position <= m_ValueLen Then m_ValueLen = l_Position + CONST_L_NG

    End If

End Function

Public Function TrimR() As StringEx

    Set TrimR = Me

    If m_ValueLen Then

        Do While InStrRev(m_Value, CONST_CHAR_32, m_ValueLen, vbBinaryCompare) = m_ValueLen

            m_ValueLen = m_ValueLen + CONST_L_NG

            If m_ValueLen = 0& Then Exit Do

        Loop

    End If

End Function

Public Function Upper() As StringEx

    Set Upper = Me

    If m_ValueLen Then String_ConvertToUpper m_ValuePtr, m_ValueLen

End Function

Private Sub BufferRedim(ByVal NewStringLen As Long, ByVal FromScratch As Boolean)

    Dim l_Buffer As String
    Dim l_HasCapacity As Boolean
    Dim l_Length As Long

    m_ValueLen = (m_ValueLen * ((Not FromScratch) * CONST_L_NG)) + NewStringLen

    If m_ValueLen > (m_ValueLenReal \ 2&) Then m_ValueLenReal = m_ValueLenReal + (m_ValueLen * 2&) Else l_HasCapacity = m_ValuePtr

    If Not l_HasCapacity Then

        If FromScratch Then

            m_Value = Space$(m_ValueLenReal)
            m_ValuePtr = StrPtr(m_Value)

        Else

            l_Length = m_ValueLen - NewStringLen

            If l_Length Then l_Buffer = VBA.Left$(m_Value, l_Length)

            m_Value = Space$(m_ValueLenReal)
            m_ValuePtr = StrPtr(m_Value)

            If l_Length Then Memory_Copy ByVal m_ValuePtr, ByVal StrPtr(l_Buffer), l_Length * 2&

        End If

    End If

End Sub

Private Function ConvertMultiByteToWideChar(ByVal lPointer As Long, ByVal lLength As Long, ByVal lBufferOffset As Long, ByVal lDataOffset As Long) As Boolean

    Dim l_Index As Long

    l_Index = String_ConvertFromUTF8(MB_ERR_INVALID_CHARS, lPointer, lLength, 0&, 0&)

    If l_Index Then

        BufferRedim l_Index - lBufferOffset, True

        ConvertMultiByteToWideChar = String_ConvertFromUTF8(0&, lPointer + lDataOffset, lLength, m_ValuePtr, m_ValueLenReal)

    End If

End Function

Private Function CryptCreate() As Boolean

    If m_CryptContext.lProvider Then

        CryptCreate = True

    Else

        CryptCreate = Cryptography_Create(App.Title, CRYPTOGRAPHY_PROVIDER_MSBASE10, CRYPT_NEWKEYSET, m_CryptContext)

    End If

End Function
