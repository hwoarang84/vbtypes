VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DateTimeExTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITest

'VARIABLES

Private m_Object As DateTimeEx
Private m_Value As Date

'EVENTS

Private Sub Class_Initialize()

    Set m_Object = New DateTimeEx

End Sub

Private Sub Class_Terminate()

    Set m_Object = Nothing

End Sub

'PROPERTIES

Private Property Get ITest_Object() As Object

    Set ITest_Object = m_Object

End Property

'METHODS

Private Sub ITest_Test(This As ITestSuite)

    TestValueDependentMethods1 This

    TestValueDependentMethods2 This

    TestValueIndependentMethods This

End Sub

Private Sub ITest_SetUp(Member As String, ByVal CallType As Long)

    m_Object.Value = m_Value

End Sub

Private Sub ITest_TearDown(Member As String, ByVal CallType As Long)
    '
End Sub

Private Sub TestValueDependentMethods1(ByRef This As ITestSuite)

    m_Value = 0&

    This.PropertyGet("Day").Assert.Equals 0

    This.PropertyGet("Equals").Assert(0&).Equals 0&

    This.PropertyGet("HasDatePortion").Assert.IsFalse

    This.PropertyGet("HasTimePortion").Assert.IsFalse

    This.PropertyGet("Hour").Assert.Equals 0

    This.PropertyGet("IsLeap").Assert.IsFalse

    This.PropertyGet("Millisecond").Assert.Equals 0

    This.PropertyGet("Minute").Assert.Equals 0

    This.PropertyGet("Month").Assert.Equals 0

    This.PropertyGet("MonthDays").Assert.Equals 0

    This.PropertyGet("Second").Assert.Equals 0

    This.PropertyGet("Value").Assert.Equals CDate(0)

    This.PropertyGet("Weekday").Assert.Equals 0

    This.PropertyGet("Year").Assert.Equals 0

    This.PropertyGet("YearDayIndex").Assert.Equals 0&

    TestHasProperValue This.Method("AddDays").Assert(-1), CDate(0)

    TestHasProperValue This.Method("AddDays").Assert(1&), DateTime.DateSerial(1899, 12, 31)

    TestHasProperValue This.Method("AddDays").Assert(10&), DateTime.DateSerial(1900, 1, 9)

    TestHasProperValue This.Method("AddHours").Assert(-1), CDate(DateTime.DateSerial(1899, 12, 29) & " " & DateTime.TimeSerial(23, 0, 0))

    TestHasProperValue This.Method("AddHours").Assert(10&), DateTime.TimeSerial(10, 0, 0)

    TestHasProperValue This.Method("AddHours").Assert(1000&), CDate(DateTime.DateSerial(1900, 2, 9) & " " & DateTime.TimeSerial(16, 0, 0))

    TestHasProperValue This.Method("AddMilliseconds").Assert(1000002), CDate(0)

    TestHasProperValue This.Method("AddMinutes").Assert(-1), CDate(DateTime.DateSerial(1899, 12, 29) & " " & DateTime.TimeSerial(23, 59, 0))

    TestHasProperValue This.Method("AddMinutes").Assert(10&), DateTime.TimeSerial(0, 10, 0)

    TestHasProperValue This.Method("AddMinutes").Assert(1000000), CDate(DateTime.DateSerial(1901, 11, 24) & " " & DateTime.TimeSerial(10, 40, 0))

    TestHasProperValue This.Method("AddMonths").Assert(-1), CDate(0)

    TestHasProperValue This.Method("AddMonths").Assert(1002&), DateTime.DateSerial(1983, 6, 30)

    TestHasProperValue This.Method("AddQuarters").Assert(-1), CDate(0)

    TestHasProperValue This.Method("AddQuarters").Assert(2&), DateTime.DateSerial(1900, 6, 30)

    TestHasProperValue This.Method("AddSeconds").Assert(-1), CDate(DateTime.DateSerial(1899, 12, 29) & " " & DateTime.TimeSerial(23, 59, 59))

    TestHasProperValue This.Method("AddSeconds").Assert(2&), DateTime.TimeSerial(0, 0, 2)

    TestHasProperValue This.Method("AddYears").Assert(-1), CDate(0)

    TestHasProperValue This.Method("AddYears").Assert(99&), DateTime.DateSerial(1998, 12, 30)

    TestHasProperValue This.Method("Clone").Assert, DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("DateSerial").Assert(1984, 8, 11), DateTime.DateSerial(1984, 8, 11)

    TestHasProperValue This.Method("TimeSerial").Assert(10, 1, 23, 576), DateTime.TimeSerial(10, 1, 23)

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals CByte(0)

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.IsFalse

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(0)

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals CCur(0)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(0)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(0)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(0)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 0

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(0)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(0)

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals CStr(DateTime.TimeSerial(0, 0, 0))

End Sub

Private Sub TestValueDependentMethods2(ByRef This As ITestSuite)

    m_Value = DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59)

    This.PropertyGet("Day").Assert.Equals 11

    This.PropertyGet("Equals").Assert(CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))).Equals 0&

    This.PropertyGet("HasDatePortion").Assert.IsTrue

    This.PropertyGet("HasTimePortion").Assert.IsTrue

    This.PropertyGet("Hour").Assert.Equals 23

    This.PropertyGet("IsLeap").Assert.IsTrue

    This.PropertyGet("Millisecond").Assert.Equals 0

    This.PropertyGet("Minute").Assert.Equals 44

    This.PropertyGet("Month").Assert.Equals 8

    This.PropertyGet("MonthDays").Assert.Equals 31

    This.PropertyGet("Second").Assert.Equals 59

    This.PropertyGet("Value").Assert.Equals CDate(CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59)))

    This.PropertyGet("Weekday").Assert.Equals 6

    This.PropertyGet("Year").Assert.Equals 1984

    This.PropertyGet("YearDayIndex").Assert.Equals 224&

    TestHasProperValue This.Method("AddDays").Assert(-1), CDate(DateTime.DateSerial(1984, 8, 10) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddDays").Assert(1&), CDate(DateTime.DateSerial(1984, 8, 12) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddDays").Assert(10&), CDate(DateTime.DateSerial(1984, 8, 21) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddHours").Assert(-1), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(22, 44, 59))

    TestHasProperValue This.Method("AddHours").Assert(10&), CDate(DateTime.DateSerial(1984, 8, 12) & " " & DateTime.TimeSerial(9, 44, 59))

    TestHasProperValue This.Method("AddHours").Assert(1000&), CDate(DateTime.DateSerial(1984, 9, 22) & " " & DateTime.TimeSerial(15, 44, 59))

    TestHasProperValue This.Method("AddMilliseconds").Assert(1002&), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 45, 0))

    TestHasProperValue This.Method("AddMinutes").Assert(-1), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 43, 59))

    TestHasProperValue This.Method("AddMinutes").Assert(10&), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 54, 59))

    TestHasProperValue This.Method("AddMinutes").Assert(1000000), CDate(DateTime.DateSerial(1986, 7, 7) & " " & DateTime.TimeSerial(10, 24, 59))

    TestHasProperValue This.Method("AddMonths").Assert(-1), CDate(DateTime.DateSerial(1984, 7, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddMonths").Assert(1002&), CDate(DateTime.DateSerial(2068, 2, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddQuarters").Assert(-1), CDate(DateTime.DateSerial(1984, 5, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddQuarters").Assert(2&), CDate(DateTime.DateSerial(1985, 2, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddSeconds").Assert(-1), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 58))

    TestHasProperValue This.Method("AddSeconds").Assert(2&), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 45, 1))

    TestHasProperValue This.Method("AddYears").Assert(-1), CDate(DateTime.DateSerial(1983, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("AddYears").Assert(99&), CDate(DateTime.DateSerial(2083, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("Clone").Assert, CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("DateSerial").Assert(2020, 8, 11), CDate(DateTime.DateSerial(2020, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("TimeSerial").Assert(10, 1, 23, 576), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(10, 1, 23))

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals CByte(192)

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.IsTrue

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(255)

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals 30905.9896@

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(30905.9895717593)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.LessOrEquals 30905.9895717593

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(30906)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(30906)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals 30905.99!

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals CStr(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))

End Sub

Private Sub TestValueIndependentMethods(ByRef This As ITestSuite)

    This.PropertyGet("Kind").Assert.Equals vbDate

    This.PropertyGet("Max").Assert.Equals DateTime.DateSerial(9999, 12, 31) & " " & DateTime.TimeSerial(23, 59, 59)

    This.PropertyGet("Min").Assert.Equals DateTime.DateSerial(100, 1, 1)

    This.PropertyGet("Pointer").Assert.NotEquals 0&

    This.PropertyGet("Size").Assert.Equals 8&

    TestHasProperValue This.Method("Now").Assert, DateTime.Now

    TestHasProperValue This.Method("Parse").Assert(AnyArrayOfVariant), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(True), CDate(0)

    TestHasProperValue This.Method("Parse").Assert(False), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(DateSerial(2000, 1, 1)), DateTime.DateSerial(2000, 1, 1)

    TestHasProperValue This.Method("Parse").Assert(CCur(922337203685477.5807@)), DateTime.DateSerial(9999, 12, 31) & " " & DateTime.TimeSerial(23, 59, 59)

    TestHasProperValue This.Method("Parse").Assert(CDec(-2.0467)), DateTime.DateSerial(1899, 12, 28) & " " & DateTime.TimeSerial(1, 7, 15)

    TestHasProperValue This.Method("Parse").Assert(CDec(0)), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(-2&), CDate(0)

    TestHasProperValue This.Method("Parse").Assert(0&), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(2), DateTime.DateSerial(1900, 1, 1)

    TestHasProperValue This.Method("Parse").Assert(0), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(DateTime.DateSerial(1984, 8, 11)), DateTime.DateSerial(1984, 8, 11)

    TestHasProperValue This.Method("Parse").Assert(CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))), CDate(DateTime.DateSerial(1984, 8, 11) & " " & DateTime.TimeSerial(23, 44, 59))

    TestHasProperValue This.Method("Parse").Assert("test"), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(vbNullString), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(Empty), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(Null), DateTime.TimeSerial(0, 0, 0)

    TestHasProperValue This.Method("Parse").Assert(This), DateTime.TimeSerial(0, 0, 0)

End Sub

Private Sub TestHasProperValue(ByRef Assertion As ITestSuiteAssert, ByVal Value As Date)

    With Assertion.IsObjectOfType(TypeName(m_Object))
        .PropertyGet("Value").Assert.Equals Value
    End With

End Sub
