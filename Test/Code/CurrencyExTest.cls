VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CurrencyExTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITest

'VARIABLES

Private m_Object As CurrencyEx
Private m_Value As Currency

'EVENTS

Private Sub Class_Initialize()

    Set m_Object = New CurrencyEx

End Sub

Private Sub Class_Terminate()

    Set m_Object = Nothing

End Sub

'PROPERTIES

Private Property Get ITest_Object() As Object

    Set ITest_Object = m_Object

End Property

'METHODS

Private Sub ITest_Test(This As ITestSuite)

    TestValueDependentMethods1 This

    TestValueDependentMethods2 This

    TestValueIndependentMethods This

End Sub

Private Sub ITest_SetUp(Member As String, ByVal CallType As Long)

    m_Object.Value = m_Value

End Sub

Private Sub ITest_TearDown(Member As String, ByVal CallType As Long)
    '
End Sub

Private Sub TestValueDependentMethods1(ByRef This As ITestSuite)

    m_Value = 0@

    This.PropertyGet("Equals").Assert(0@).Equals 0&

    This.PropertyGet("Value").Assert.Equals 0@

    TestHasProperValue This.Method("Clone").Assert, 0@

    TestHasProperValue This.Method("Decrement").Assert, -1@

    TestHasProperValue This.Method("Increment").Assert, 1@

    TestHasProperValue This.Method("Range").Assert(-4515.4532, 100.08), 0@

    TestHasProperValue This.Method("Round").Assert(2&), 0@

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 0

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.IsFalse

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(0)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals CDate(0)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(0)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(0)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(0)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 0

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(0)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(0)

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "0"

End Sub

Private Sub TestValueDependentMethods2(ByRef This As ITestSuite)

    m_Value = -3.0254@

    This.PropertyGet("Equals").Assert(-3.0254@).Equals 0&

    This.PropertyGet("Value").Assert.Equals -3.0254@

    TestHasProperValue This.Method("Clone").Assert, -3.0254@

    TestHasProperValue This.Method("Decrement").Assert, -4.0254@

    TestHasProperValue This.Method("Increment").Assert, -2.0254@

    TestHasProperValue This.Method("Range").Assert(-2.4532, 2.08), -2.4532@

    TestHasProperValue This.Method("Range").Assert(-4515.4532, 100.08), -3.0254@

    TestHasProperValue This.Method("Round").Assert(0&), -3@

    TestHasProperValue This.Method("Round").Assert(2&), -3.03@

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 210

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.IsTrue

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(0)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals DateSerial(1899, 12, 27) & " " & TimeSerial(0, 36, 35)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(-3.0254)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(-3.0254)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(-3)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals -3.0254@

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(-3)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(-3.0254)

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "-3,0254"

End Sub

Private Sub TestValueIndependentMethods(ByRef This As ITestSuite)

    This.PropertyGet("Kind").Assert.Equals vbCurrency

    This.PropertyGet("Max").Assert.Equals 922337203685477.5807@

    This.PropertyGet("Min").Assert.Equals -922337203685477.5807@

    This.PropertyGet("Pointer").Assert.NotEquals 0&

    This.PropertyGet("Size").Assert.Equals 8&

    TestHasProperValue This.Method("Parse").Assert(AnyArrayOfVariant), 0@

    TestHasProperValue This.Method("Parse").Assert(True), -1@

    TestHasProperValue This.Method("Parse").Assert(False), 0@

    TestHasProperValue This.Method("Parse").Assert(DateSerial(2000, 1, 1)), 36526@

    TestHasProperValue This.Method("Parse").Assert(CDec(-2.0467)), -2.0467@

    TestHasProperValue This.Method("Parse").Assert(CDec(0)), 0@

    TestHasProperValue This.Method("Parse").Assert(-2&), -2@

    TestHasProperValue This.Method("Parse").Assert(0&), 0@

    TestHasProperValue This.Method("Parse").Assert(2), 2@

    TestHasProperValue This.Method("Parse").Assert(0), 0@

    TestHasProperValue This.Method("Parse").Assert("-1056d"), -1056

    TestHasProperValue This.Method("Parse").Assert("0d"), 0@

    TestHasProperValue This.Method("Parse").Assert("test"), 0@

    TestHasProperValue This.Method("Parse").Assert(vbNullString), 0@

    TestHasProperValue This.Method("Parse").Assert(Empty), 0@

    TestHasProperValue This.Method("Parse").Assert(Null), 0@

    TestHasProperValue This.Method("Parse").Assert(This), 0@

End Sub

Private Sub TestHasProperValue(ByRef Assertion As ITestSuiteAssert, ByVal Value As Currency)

    With Assertion.IsObjectOfType(TypeName(m_Object))
        .PropertyGet("Value").Assert.Equals Value
    End With

End Sub
