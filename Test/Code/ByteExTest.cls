VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ByteExTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITest

'VARIABLES

Private m_Object As ByteEx
Private m_Value As Byte

'EVENTS

Private Sub Class_Initialize()

    Set m_Object = New ByteEx

End Sub

Private Sub Class_Terminate()

    Set m_Object = Nothing

End Sub

'PROPERTIES

Private Property Get ITest_Object() As Object

    Set ITest_Object = m_Object

End Property

'METHODS

Private Sub ITest_Test(This As ITestSuite)

    TestValueDependentMethods1 This

    TestValueDependentMethods2 This

    TestValueIndependentMethods This

End Sub

Private Sub ITest_SetUp(Member As String, ByVal CallType As Long)

    m_Object.Value = m_Value

End Sub

Private Sub ITest_TearDown(Member As String, ByVal CallType As Long)
    '
End Sub

Private Sub TestValueDependentMethods1(ByRef This As ITestSuite)

    m_Value = 0

    This.PropertyGet("Equals").Assert(0).Equals 0&

    This.PropertyGet("Value").Assert.Equals 0

    TestHasProperValue This.Method("Clone").Assert, 0

    TestHasProperValue This.Method("Decrement").Assert, 0

    TestHasProperValue This.Method("Increment").Assert, 1

    TestHasProperValue This.Method("Range").Assert(0, 100), 0

    TestHasProperValue This.Method("Range").Assert(10, 15), 10

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 0

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.IsFalse

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals CCur(0)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals CDate(0)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(0)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(0)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(0)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 0

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(0)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(0)

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "0"

End Sub

Private Sub TestValueDependentMethods2(ByRef This As ITestSuite)

    m_Value = 254

    This.PropertyGet("Equals").Assert(254).Equals 0&

    This.PropertyGet("Value").Assert.Equals 254

    TestHasProperValue This.Method("Clone").Assert, 254

    TestHasProperValue This.Method("Decrement").Assert, 253

    TestHasProperValue This.Method("Increment").Assert, 255

    TestHasProperValue This.Method("Range").Assert(0, 100), 100

    TestHasProperValue This.Method("Range").Assert(10, 15), 15

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 254

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.Equals True

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals CCur(254)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals CDate(254)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(254)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(254)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(254)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).Equals 254

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(254)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(254)

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "254"

    This.Method("ToString").Assert("##,##0.0").IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "254,0"

End Sub

Private Sub TestValueIndependentMethods(ByRef This As ITestSuite)

    This.PropertyGet("Kind").Assert.Equals vbByte

    This.PropertyGet("Max").Assert.Equals 255

    This.PropertyGet("Min").Assert.Equals 0

    This.PropertyGet("Pointer").Assert.NotEquals 0&

    This.PropertyGet("Size").Assert.Equals 1&

    TestHasProperValue This.Method("Parse").Assert(AnyArrayOfVariant), 0

    TestHasProperValue This.Method("Parse").Assert(True), 255

    TestHasProperValue This.Method("Parse").Assert(False), 0

    TestHasProperValue This.Method("Parse").Assert(DateSerial(2000, 1, 1)), 255

    TestHasProperValue This.Method("Parse").Assert(CDec(-2.0467)), 0

    TestHasProperValue This.Method("Parse").Assert(CDec(1.0003)), 1

    TestHasProperValue This.Method("Parse").Assert(CDec(0)), 0

    TestHasProperValue This.Method("Parse").Assert(-2&), 0

    TestHasProperValue This.Method("Parse").Assert(2&), 2

    TestHasProperValue This.Method("Parse").Assert(0#), 0

    TestHasProperValue This.Method("Parse").Assert(-2&), 0

    TestHasProperValue This.Method("Parse").Assert(2&), 2

    TestHasProperValue This.Method("Parse").Assert(0&), 0

    TestHasProperValue This.Method("Parse").Assert("-1d"), 0

    TestHasProperValue This.Method("Parse").Assert("34d"), 34

    TestHasProperValue This.Method("Parse").Assert("test"), 0

    TestHasProperValue This.Method("Parse").Assert(vbNullString), 0

    TestHasProperValue This.Method("Parse").Assert(Empty), 0

    TestHasProperValue This.Method("Parse").Assert(Null), 0

    TestHasProperValue This.Method("Parse").Assert(This), 0

End Sub

Private Sub TestHasProperValue(ByRef Assertion As ITestSuiteAssert, ByVal Value As Byte)

    With Assertion.IsObjectOfType(TypeName(m_Object))
        .PropertyGet("Value").Assert.Equals Value
    End With

End Sub
