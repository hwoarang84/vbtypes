VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StringExTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITest

'VARIABLES

Private m_Object As StringEx
Private m_Value As String

'EVENTS

Private Sub Class_Initialize()

    Set m_Object = New StringEx

End Sub

Private Sub Class_Terminate()

    Set m_Object = Nothing

End Sub

'PROPERTIES

Private Property Get ITest_Object() As Object

    Set ITest_Object = m_Object

End Property

'METHODS

Private Sub ITest_Test(This As ITestSuite)

    TestValueDependentMethods1 This

    TestValueDependentMethods2 This

    TestValueIndependentMethods This

End Sub

Private Sub ITest_SetUp(Member As String, ByVal CallType As Long)

    m_Object.Value = m_Value

End Sub

Private Sub ITest_TearDown(Member As String, ByVal CallType As Long)
    '
End Sub

Private Sub TestValueDependentMethods1(ByRef This As ITestSuite)

    m_Value = vbNullString

    This.PropertyGet("Asc").Assert(AnyLong).Equals 0&

    This.PropertyGet("Capacity").Assert.Equals 128&

    This.PropertyGet("Contains").Assert(AnyString).IsFalse

    This.PropertyGet("Equals").Assert(AnyVariant).NotEquals 0&

    This.PropertyGet("IndexOf").Assert(AnyString, AnyLong, AnyBoolean).Equals 0&

    This.PropertyGet("IsEmptyOrWhiteSpace").Assert.IsTrue

    This.PropertyGet("Length").Assert.Equals 0&

    This.PropertyGet("Pointer").Assert.Equals 0&

    This.PropertyGet("Value").Assert.Equals vbNullString

    This.PropertyLet("Value").Assert(AnyString).ThrowsNothing

    TestHasProperValue This.Method("Clone").Assert, vbNullString

    TestHasProperValue This.Method("Concat").Assert("string"), "string"

    TestHasProperValue This.Method("ConcatPointer").Assert(StrPtr("string")), "string"

    TestHasProperValue This.Method("Duplicate").Assert(4&), vbNullString

    TestHasProperValue This.Method("Encrypt").Assert.IsObjectOfType("StringEx").Method("Decrypt").Assert, vbNullString

    TestHasProperValue This.Method("Insert").Assert(0&, "string"), "string"

    TestHasProperValue This.Method("Left").Assert(AnyLong), vbNullString

    TestHasProperValue This.Method("Lower").Assert, vbNullString

    TestHasProperValue This.Method("Mid").Assert(AnyLong), vbNullString

    TestHasProperValue This.Method("Numeric").Assert, "0"

    TestHasProperValue This.Method("PadLeft").Assert(10&), Space$(10&)

    TestHasProperValue This.Method("PadRight").Assert(10&), Space$(10&)

    TestHasProperValue This.Method("Remove").Assert(0&, 1&), vbNullString

    TestHasProperValue This.Method("Replace").Assert(AnyString, AnyString), vbNullString

    TestHasProperValue This.Method("Right").Assert(AnyLong), vbNullString

    This.Method("Split").Assert(",", 10&, vbBinaryCompare, vbLong).IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 0&

    This.Method("ToArray").Assert(False).IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 0&

    This.Method("ToArray").Assert(True).IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 0&

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.IsFalse

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(0)

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals CCur(0)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals CDate(0)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(0)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(0)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(0)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 0&

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(0)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(0)

    TestHasProperValue This.Method("TrimL").Assert, vbNullString

    TestHasProperValue This.Method("TrimNull").Assert, vbNullString

    TestHasProperValue This.Method("TrimR").Assert, vbNullString

    TestHasProperValue This.Method("Upper").Assert, vbNullString

End Sub

Private Sub TestValueDependentMethods2(ByRef This As ITestSuite)

    m_Value = " test String, 0.45   "

    This.PropertyGet("Asc") _
       .Assert(1&).Equals(32&) _
       .Assert(7&).Equals 83&

    This.PropertyGet("Capacity").Assert.Equals 128&

    This.PropertyGet("Contains") _
       .Assert("test string", vbBinaryCompare).IsFalse _
       .Assert("test String", vbBinaryCompare).IsTrue _
       .Assert("test string", vbTextCompare).IsTrue

    This.PropertyGet("Equals") _
       .Assert(" test String, 0.45   ").Equals(0&) _
       .Assert(" test string, 0.45   ").NotEquals(0&) _
       .Assert(AnyString).NotEquals 0&

    This.PropertyGet("IndexOf") _
       .Assert("test string", 1&, False, vbBinaryCompare).Equals(0&) _
       .Assert("test String", 1&, False, vbBinaryCompare).Equals(2&) _
       .Assert("test string", 1&, False, vbTextCompare).Equals(2&) _
       .Assert("test string", 0&, True, vbTextCompare).Equals 2&

    This.PropertyGet("IsEmptyOrWhiteSpace").Assert.IsFalse

    This.PropertyGet("Length").Assert.Equals 21&

    This.PropertyGet("Pointer").Assert.Greater 0&

    This.PropertyGet("Value").Assert.Equals " test String, 0.45   "

    This.PropertyLet("Value").Assert(AnyString).ThrowsNothing

    TestHasProperValue This.Method("Clone").Assert, " test String, 0.45   "

    TestHasProperValue This.Method("Concat").Assert("string"), " test String, 0.45   string"

    TestHasProperValue This.Method("ConcatPointer").Assert(StrPtr("string")), " test String, 0.45   string"

    TestHasProperValue This.Method("Duplicate").Assert(3&), " test String, 0.45    test String, 0.45    test String, 0.45    test String, 0.45   "

    TestHasProperValue This.Method("Encrypt").Assert.IsObjectOfType("StringEx").Method("Decrypt").Assert, " test String, 0.45   "

    TestHasProperValue This.Method("Insert").Assert(3&, "string"), " testringst String, 0.45   "

    TestHasProperValue This.Method("Left").Assert(7&), " test S"

    TestHasProperValue This.Method("Lower").Assert, " test string, 0.45   "

    TestHasProperValue This.Method("Mid").Assert(7&, 3&), "Str"

    TestHasProperValue This.Method("Numeric").Assert, ",045"

    TestHasProperValue This.Method("PadLeft").Assert(30&), "          test String, 0.45   "

    TestHasProperValue This.Method("PadRight").Assert(30&), " test String, 0.45            "

    TestHasProperValue This.Method("Remove").Assert(7&, 3&), " test ing, 0.45   "

    TestHasProperValue This.Method("Replace").Assert("test string", vbNullString, 1&, -1&, vbBinaryCompare), " test String, 0.45   "

    TestHasProperValue This.Method("Replace").Assert("test String", vbNullString, 1&, -1&, vbBinaryCompare), " , 0.45   "

    TestHasProperValue This.Method("Replace").Assert("test string", vbNullString, 1&, -1&, vbTextCompare), " , 0.45   "

    TestHasProperValue This.Method("Right").Assert(5&), "45   "

    This.Method("Split").Assert(",", 10&, vbBinaryCompare, vbString).IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 2&

    This.Method("ToArray").Assert(False).IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 42&

    This.Method("ToArray").Assert(True).IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 21&

    This.Method("ToBoolean").Assert.IsObjectOfType("BooleanEx").PropertyGet("Value").Assert.IsTrue

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(0)

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals CCur(0.045)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals CDate(0)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(0.045)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(0.045)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(0)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Count").Assert.Equals 1&

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(0)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(0.045)

    TestHasProperValue This.Method("TrimL").Assert, "test String, 0.45   "

    TestHasProperValue This.Method("TrimNull").Assert, " test String, 0.45   "

    TestHasProperValue This.Method("TrimR").Assert, " test String, 0.45"

    TestHasProperValue This.Method("Upper").Assert, " TEST STRING, 0.45   "

End Sub

Private Sub TestValueIndependentMethods(ByRef This As ITestSuite)

    Dim l_Array() As Byte

    This.PropertyGet("HashCode").Assert.NotEquals 0&

    This.PropertyGet("Kind").Assert.Equals vbString

    This.PropertyGet("Size").Assert.Equals 4&

    ReDim l_Array(7): l_Array(0) = 239: l_Array(1) = 187: l_Array(2) = 191: l_Array(3) = 72: l_Array(4) = 101: l_Array(5) = 108: l_Array(6) = 108: l_Array(7) = 111
    TestHasProperValue This.Method("Parse").Assert(l_Array), "Hello"

    ReDim l_Array(4): l_Array(0) = 72: l_Array(1) = 101: l_Array(2) = 108: l_Array(3) = 108: l_Array(4) = 111
    TestHasProperValue This.Method("Parse").Assert(l_Array), "Hello"

    ReDim l_Array(11): l_Array(0) = 255: l_Array(1) = 254: l_Array(2) = 72: l_Array(4) = 101:  l_Array(6) = 108:  l_Array(8) = 108:  l_Array(10) = 111
    TestHasProperValue This.Method("Parse").Assert(l_Array), "Hello"

    ReDim l_Array(11): l_Array(0) = 254: l_Array(1) = 255: l_Array(3) = 72: l_Array(5) = 101:  l_Array(7) = 108:  l_Array(9) = 108:  l_Array(11) = 111
    TestHasProperValue This.Method("Parse").Assert(l_Array), "Hello"

    ReDim l_Array(9): l_Array(0) = 72: l_Array(2) = 101: l_Array(4) = 108: l_Array(6) = 108: l_Array(8) = 111
    TestHasProperValue This.Method("Parse").Assert(l_Array), "Hello"

    TestHasProperValue This.Method("Parse").Assert(AnyArrayOfVariant), vbNullString

    TestHasProperValue This.Method("Parse").Assert(True), "True"

    TestHasProperValue This.Method("Parse").Assert(False), "False"

    TestHasProperValue This.Method("Parse").Assert(DateSerial(2000, 1, 1)), "01.01.2000"

    TestHasProperValue This.Method("Parse").Assert(CDec(-2.0467)), "-2,0467"

    TestHasProperValue This.Method("Parse").Assert(-2&), "-2"

    TestHasProperValue This.Method("Parse").Assert(-2), "-2"

    TestHasProperValue This.Method("Parse").Assert(-2.95!), "-2,95"

    TestHasProperValue This.Method("Parse").Assert(vbNullString), vbNullString

    TestHasProperValue This.Method("Parse").Assert(Empty), vbNullString

    TestHasProperValue This.Method("Parse").Assert(Null), vbNullString

    TestHasProperValue This.Method("Parse").Assert(This), vbNullString

End Sub

Private Sub TestHasProperValue(ByRef Assertion As ITestSuiteAssert, ByRef Value As String)

    With Assertion.IsObjectOfType(TypeName(m_Object))
        .PropertyGet("Value").Assert.Equals Value
    End With

End Sub
