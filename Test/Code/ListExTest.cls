VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ListExTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITest

'VARIABLES

Private m_Object As ListEx
Private m_Value As Variant

'EVENTS

Private Sub Class_Initialize()

    Set m_Object = New ListEx

End Sub

Private Sub Class_Terminate()

    Set m_Object = Nothing

End Sub

'PROPERTIES

Private Property Get ITest_Object() As Object

    Set ITest_Object = m_Object

End Property

'METHODS

Private Sub ITest_Test(This As ITestSuite)

    TestValueDependentMethods1 This

    TestValueDependentMethods2 This

    TestValueIndependentMethods This

End Sub

Private Sub ITest_SetUp(Member As String, ByVal CallType As Long)

    m_Object.Value = m_Value

End Sub

Private Sub ITest_TearDown(Member As String, ByVal CallType As Long)
    '
End Sub

Private Sub TestValueDependentMethods1(ByRef This As ITestSuite)

    m_Value = Empty

    This.PropertyGet("Count").Assert.Equals 1&

    This.PropertyGet("Data").Assert.NotEquals 0&

    This.PropertyGet("ElementSize").Assert.Equals 16&

    This.PropertyGet("ElementType").Assert.Equals vbVariant

    This.PropertyGet("Equals").Assert(Array(Empty)).Equals 0&

    This.PropertyGet("Exists").Assert.IsTrue

    This.PropertyGet("First").Assert.IsEmpty

    This.PropertyGet("IndexOf").Assert(Empty).Equals 0&

    This.PropertyGet("Item").Assert(AnyLong).IsEmpty

    This.PropertyLet("Item").Assert(AnyLong, AnyVariant).ThrowsNothing

    This.PropertyGet("ItemExists").Assert(0&).IsTrue

    This.PropertyGet("Kind").Assert.Equals vbArray + vbVariant

    This.PropertyGet("Last").Assert.IsEmpty

    This.PropertyGet("Pointer").Assert.NotEquals 0&

    This.PropertyGet("Size").Assert.Equals 16&

    This.PropertyGet("Sorted").Assert.Equals 0&

    This.PropertyGet("Value").Assert.Equals Array(Empty)

    This.PropertyLet("Value") _
       .Assert(AnyArrayOfVariant).ThrowsNothing _
       .Assert(AnyArrayOfVariant(3&)).ThrowsNothing _
       .Assert(AnyVariant).ThrowsNothing

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfBoolean), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfBoolean(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfByte), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfByte(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfCurrency), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfCurrency(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDate), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDate(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDouble), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDouble(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfInteger), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfInteger(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfLong), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfLong(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfSingle), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfSingle(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfString), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfString(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfVariant), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfVariant(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyBoolean), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyByte), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyCurrency), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyDate), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyDouble), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyInteger), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyLong), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnySingle), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyString), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(Empty), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(Null), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(This), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfBoolean), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfBoolean(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfByte), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfByte(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfCurrency), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfCurrency(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDate), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDate(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDouble), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDouble(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfInteger), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfInteger(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfLong), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfLong(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfSingle), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfSingle(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfString), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfString(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfVariant), vbVariant, 11&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfVariant(3&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyBoolean), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyByte), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyCurrency), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyDate), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyDouble), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyInteger), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyLong), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnySingle), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyString), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(Empty), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(Null), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(This), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Clone").Assert, vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Concat").Assert(NewList(13&)), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Distinct").Assert, vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Filter").Assert(Empty), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Filter").Assert(1&), vbVariant, 0&

    ArrayHasProperTypeAndElements This.Method("FilterAt").Assert(0&), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("FilterAt").Assert(1&), vbVariant, 0&

    ArrayHasProperTypeAndElements This.Method("Range").Assert(0&, 0&), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Range").Assert(0&, 10&), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Remove").Assert(Empty), vbVariant, 0&

    ArrayHasProperTypeAndElements This.Method("RemoveAt").Assert(0&), vbVariant, 0&

    ArrayHasProperTypeAndElements This.Method("Resize").Assert(0&), vbVariant, 0&

    ArrayHasProperTypeAndElements This.Method("Reverse").Assert, vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Sort").Assert, vbVariant, 1&

    This.Method("ToString").Assert(0&, 0&).IsObjectOfType("StringEx").PropertyGet("Length").Assert.Equals 0&

End Sub

Private Sub TestValueDependentMethods2(ByRef This As ITestSuite)

    ReDim m_Value(-1 To 3) As Long

    m_Value(-1) = 11&: m_Value(0) = 56&: m_Value(1) = 762&: m_Value(2) = 11&: m_Value(3) = -135&

    This.PropertyGet("Count").Assert.Equals 5&

    This.PropertyGet("Data").Assert.NotEquals 0&

    This.PropertyGet("ElementSize").Assert.Equals 4&

    This.PropertyGet("ElementType").Assert.Equals vbLong

    This.PropertyGet("Equals").Assert(m_Value).Equals 0&

    This.PropertyGet("Exists").Assert.IsTrue

    This.PropertyGet("First").Assert.Equals 11&

    This.PropertyGet("IndexOf").Assert(762&).Equals 2&

    This.PropertyGet("Item").Assert(3&).Equals 11&

    This.PropertyLet("Item").Assert(AnyLong, AnyVariant).ThrowsNothing

    This.PropertyGet("ItemExists").Assert(0&).IsTrue

    This.PropertyGet("Kind").Assert.Equals vbArray + vbLong

    This.PropertyGet("Last").Assert.Equals -135&

    This.PropertyGet("Pointer").Assert.NotEquals 0&

    This.PropertyGet("Size").Assert.Equals 20&

    This.PropertyGet("Sorted").Assert.Equals 0&

    This.PropertyGet("Value").Assert.Equals m_Value

    This.PropertyLet("Value") _
       .Assert(AnyArrayOfVariant).ThrowsNothing _
       .Assert(AnyArrayOfVariant(3&)).ThrowsNothing _
       .Assert(AnyVariant).ThrowsNothing

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfBoolean), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfBoolean(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfByte), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfByte(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfCurrency), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfCurrency(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDate), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDate(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDouble), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfDouble(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfInteger), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfInteger(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfLong), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfLong(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfSingle), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfSingle(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfString), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfString(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfVariant), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyArrayOfVariant(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyBoolean), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyByte), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyCurrency), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyDate), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyDouble), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyInteger), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyLong), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnySingle), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(AnyString), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(Empty), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(Null), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Add").Assert(This), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfBoolean), vbLong, 15&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfBoolean(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfByte), vbLong, 15&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfByte(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfCurrency), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfCurrency(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDate), vbLong, 15&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDate(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDouble), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfDouble(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfInteger), vbLong, 15&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfInteger(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfLong), vbLong, 15&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfLong(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfSingle), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfSingle(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfString), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfString(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyArrayOfVariant(3&)), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyBoolean), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyByte), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyCurrency), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyDate), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyDouble), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyInteger), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyLong), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnySingle), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(AnyString), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(Empty), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(Null), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("AddRange").Assert(This), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Clone").Assert, vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Concat").Assert(NewList(13&)), vbLong, 6&

    ArrayHasProperTypeAndElements This.Method("Distinct").Assert, vbLong, 4&

    ArrayHasProperTypeAndElements This.Method("Filter").Assert(11&), vbLong, 2&

    ArrayHasProperTypeAndElements This.Method("Filter").Assert(Array(11&, 762&)), vbLong, 3&

    ArrayHasProperTypeAndElements This.Method("Filter").Assert(33&), vbLong, 0&

    ArrayHasProperTypeAndElements This.Method("FilterAt").Assert(0&), vbLong, 1&

    ArrayHasProperTypeAndElements This.Method("FilterAt").Assert(Array(0&, 3&)), vbLong, 2&

    ArrayHasProperTypeAndElements This.Method("FilterAt").Assert(7&), vbLong, 0&

    ArrayHasProperTypeAndElements This.Method("Range").Assert(1&, 3&), vbLong, 3&

    ArrayHasProperTypeAndElements This.Method("Range").Assert(0&, 10&), vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Remove").Assert(11&), vbLong, 3&

    ArrayHasProperTypeAndElements This.Method("RemoveAt").Assert(0&), vbLong, 4&

    ArrayHasProperTypeAndElements This.Method("Resize").Assert(3&), vbLong, 3&

    ArrayHasProperTypeAndElements This.Method("Reverse").Assert, vbLong, 5&

    ArrayHasProperTypeAndElements This.Method("Sort").Assert, vbLong, 5&

    This.Method("ToString").Assert(vbNullString, 0&, 4&).IsObjectOfType("StringEx").PropertyGet("Length").Assert.Equals 13&

    This.Method("ToString").Assert(",", 1&, 3&).IsObjectOfType("StringEx").PropertyGet("Length").Assert.Equals 9&

End Sub

Private Sub TestValueIndependentMethods(ByRef This As ITestSuite)

    ArrayHasProperTypeAndElements This.Method("Destroy").Assert, 0&, 0&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbBoolean, 2&), vbBoolean, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbByte, 2&), vbByte, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbCurrency, 2&), vbCurrency, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbDate, 2&), vbDate, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbDouble, 2&), vbDouble, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbInteger, 2&), vbInteger, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbLong, 2&), vbLong, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbSingle, 2&), vbSingle, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbString, 2&), vbString, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbVariant, 2&), vbVariant, 2&

    ArrayHasProperTypeAndElements This.Method("Create").Assert(vbObject, 2&), vbObject, 2&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfBoolean), vbBoolean, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfBoolean(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfByte), vbByte, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfByte(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfCurrency), vbCurrency, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfCurrency(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfDate), vbDate, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfDate(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfDouble), vbDouble, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfDouble(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfInteger), vbInteger, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfInteger(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfLong), vbLong, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfLong(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfSingle), vbSingle, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfSingle(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfString), vbString, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfString(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfVariant), vbVariant, 10&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyArrayOfVariant(3&)), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyBoolean), vbBoolean, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyByte), vbByte, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyCurrency), vbCurrency, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyDate), vbDate, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyDouble), vbDouble, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyInteger), vbInteger, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyLong), vbLong, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnySingle), vbSingle, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(AnyString), vbString, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(Empty), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(Null), vbVariant, 1&

    ArrayHasProperTypeAndElements This.Method("Parse").Assert(This), vbObject, 1&

End Sub

Private Sub ArrayHasProperTypeAndElements(ByRef Assertion As ITestSuiteAssert, ByVal VarType As VbVarType, ByVal Elements As Long)

    With Assertion.IsObjectOfType("ListEx")
        .PropertyGet("Count").Assert.Equals Elements
        .PropertyGet("ElementType").Assert.Equals VarType
    End With

End Sub
