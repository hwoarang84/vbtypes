VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BooleanExTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'INTERFACES

Implements ITest

'VARIABLES

Private m_Object As BooleanEx
Private m_Value As Boolean

'EVENTS

Private Sub Class_Initialize()

    Set m_Object = New BooleanEx

End Sub

Private Sub Class_Terminate()

    Set m_Object = Nothing

End Sub

'PROPERTIES

Private Property Get ITest_Object() As Object

    Set ITest_Object = m_Object

End Property

'METHODS

Private Sub ITest_Test(This As ITestSuite)

    TestValueDependentMethods1 This

    TestValueDependentMethods2 This

    TestValueIndependentMethods This

End Sub

Private Sub ITest_SetUp(Member As String, ByVal CallType As Long)

    m_Object.Value = m_Value

End Sub

Private Sub ITest_TearDown(Member As String, ByVal CallType As Long)
    '
End Sub

Private Sub TestValueDependentMethods1(ByRef This As ITestSuite)

    m_Value = False

    This.PropertyGet("Equals").Assert(False).Equals 0&

    This.PropertyGet("Value").Assert.IsFalse

    TestHasProperValue This.Method("Clone").Assert, False

    TestHasProperValue This.Method("Reverse").Assert, True

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(1&).Equals 0

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(0)

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals CCur(0)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals CDate(0)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(0)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(0)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(0)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).IsFalse

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(0)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(0)

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "False"

    This.Method("ToString").Assert("##,##0.0").IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "0,0"

End Sub

Private Sub TestValueDependentMethods2(ByRef This As ITestSuite)

    m_Value = True

    This.PropertyGet("Equals").Assert(True).Equals 0&

    This.PropertyGet("Value").Assert.IsTrue

    TestHasProperValue This.Method("Clone").Assert, True

    TestHasProperValue This.Method("Reverse").Assert, False

    This.Method("ToArray").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(1&).Equals 255

    This.Method("ToByte").Assert.IsObjectOfType("ByteEx").PropertyGet("Value").Assert.Equals CByte(255)

    This.Method("ToCurrency").Assert.IsObjectOfType("CurrencyEx").PropertyGet("Value").Assert.Equals CCur(-1)

    This.Method("ToDateTime").Assert.IsObjectOfType("DateTimeEx").PropertyGet("Value").Assert.Equals CDate(0)

    This.Method("ToDecimal").Assert.IsObjectOfType("DecimalEx").PropertyGet("Value").Assert.Equals CDec(-1)

    This.Method("ToDouble").Assert.IsObjectOfType("DoubleEx").PropertyGet("Value").Assert.Equals CDbl(-1)

    This.Method("ToInteger").Assert.IsObjectOfType("IntegerEx").PropertyGet("Value").Assert.Equals CInt(-1)

    This.Method("ToList").Assert.IsObjectOfType("ListEx").PropertyGet("Item").Assert(0&).IsTrue

    This.Method("ToLong").Assert.IsObjectOfType("LongEx").PropertyGet("Value").Assert.Equals CLng(-1)

    This.Method("ToSingle").Assert.IsObjectOfType("SingleEx").PropertyGet("Value").Assert.Equals CSng(-1)

    This.Method("ToString").Assert.IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "True"

    This.Method("ToString").Assert("##,##0.0").IsObjectOfType("StringEx").PropertyGet("Value").Assert.Equals "-1,0"

End Sub

Private Sub TestValueIndependentMethods(ByRef This As ITestSuite)

    This.PropertyGet("Kind").Assert.Equals vbBoolean

    This.PropertyGet("Max").Assert.IsTrue

    This.PropertyGet("Min").Assert.IsFalse

    This.PropertyGet("Pointer").Assert.NotEquals 0&

    This.PropertyGet("Size").Assert.Equals 2&

    TestHasProperValue This.Method("Parse").Assert(AnyArrayOfVariant), False

    TestHasProperValue This.Method("Parse").Assert(True), True

    TestHasProperValue This.Method("Parse").Assert(False), False

    TestHasProperValue This.Method("Parse").Assert(AnyDate), True

    TestHasProperValue This.Method("Parse").Assert(CDec(-2.0467)), True

    TestHasProperValue This.Method("Parse").Assert(CDec(1.0003)), True

    TestHasProperValue This.Method("Parse").Assert(CDec(0)), False

    TestHasProperValue This.Method("Parse").Assert(-2&), True

    TestHasProperValue This.Method("Parse").Assert(2&), True

    TestHasProperValue This.Method("Parse").Assert(0#), False

    TestHasProperValue This.Method("Parse").Assert(-2&), True

    TestHasProperValue This.Method("Parse").Assert(2&), True

    TestHasProperValue This.Method("Parse").Assert(0&), False

    TestHasProperValue This.Method("Parse").Assert("-1d"), True

    TestHasProperValue This.Method("Parse").Assert("0.0d"), False

    TestHasProperValue This.Method("Parse").Assert("True"), True

    TestHasProperValue This.Method("Parse").Assert("true"), False

    TestHasProperValue This.Method("Parse").Assert("test"), False

    TestHasProperValue This.Method("Parse").Assert(vbNullString), False

    TestHasProperValue This.Method("Parse").Assert(Empty), False

    TestHasProperValue This.Method("Parse").Assert(Null), False

    TestHasProperValue This.Method("Parse").Assert(This), False

End Sub

Private Sub TestHasProperValue(ByRef Assertion As ITestSuiteAssert, ByVal Value As Boolean)

    With Assertion.IsObjectOfType(TypeName(m_Object))

        If Value Then .PropertyGet("Value").Assert.IsTrue Else .PropertyGet("Value").Assert.IsFalse

    End With

End Sub
